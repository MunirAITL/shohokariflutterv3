import 'package:aitl/config/server/Server.dart';

class APITaskAlertCfg {
  static const String TASK_ALERT_GET_URL =
      Server.BASE_URL + "/api/taskalertkeyword/get";
  static const String TASK_ALERT_POST_URL =
      Server.BASE_URL + "/api/taskalertkeyword/post";
  static const String TASK_ALERT_PUT_URL =
      Server.BASE_URL + "/api/taskalertkeyword/put";
  static const String TASK_ALERT_DEL_URL =
      Server.BASE_URL + "/api/taskalertkeyword/delete/#alertId#";
}
