import 'package:aitl/config/server/Server.dart';

class APIPayCfg {
  static const String PAYMENTCONFIRMATION_PUT_URL =
      Server.BASE_URL + "/api/taskbidding/put/paymentconfirmation";
}
