import 'package:aitl/config/server/Server.dart';

class APIPostTaskCfg {
  static const listBannerUrl = [
    Server.BASE_URL + "/assets/img/slider/3.jpg",
    Server.BASE_URL + "/assets/img/slider/2.jpg",
    Server.BASE_URL + "/assets/img/slider/1.png",
  ];

  static const String PENDING_REVIEWRATING_BYUSERID_GET_URL = Server.BASE_URL +
      "/api/userrating/getpendingreviewuserratingbyuserid/#userId#";

  //  my tasks, find works, messages
  static const String TASKINFOBYSEARCH_POST_URL =
      Server.BASE_URL + "/api/task/taskinformationbysearch/get";

  //  add task start here...
  //  step 1
  static const String POSTTASK1_URL = Server.BASE_URL + "/api/task/post";
  static const String POSTTASK1_PUT_URL = Server.BASE_URL + "/api/task/put";

  //  save pic
  static const String SAVE_PIC_URL = Server.BASE_URL + "/api/task/savepictures";

  //  delete pic
  static const String DEL_PIC_URL =
      Server.BASE_URL + "/api/task/deletetaskimages/#mediaId#";

  static const String GET_PIC_URL =
      Server.BASE_URL + "/api/task/getpictures/#taskId#";

  //  delete task
  static const String DEL_TASK_URL =
      Server.BASE_URL + "/api/task/delete/#taskId#";

  //  step 3
  static const String POSTTASK3_URL = Server.BASE_URL + "/api/task/put";

  //  create task -> all categories -> parent and child cat
  static const String PARENT_TASK_CAT_GET_URL =
      Server.BASE_URL + "/api/usertaskcategory/getallparentcategorysortbyorder";

  static const String CHILD_TASK_CAT_GET_URL = Server.BASE_URL +
      "/api/usertaskcategory/getallchildcategoryandcategoryitembyparentid";

  static const TASK_CAT_FORM_GET_URL =
      Server.BASE_URL + "/api/usertaskitemform/getitemformbyitemid";

  static const TASK_CAT_FORM_POST_URL =
      Server.BASE_URL + "/api/task/posttaskitemform";
  //
  static const String GET_TASK_SUMMARY_USERDATA_URL =
      Server.BASE_URL + "/api/task/gettasksummaryuserdata";
}
