import 'package:aitl/config/theme/MyTheme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class ConfirmOferController extends GetxController {
  var listOffers = [
    {
      "icon": Icons.account_box_outlined,
      "title": "confirm_your_offer_add_profile_picture".tr,
      "status_icon": SvgPicture.asset("assets/images/svg/ico_ok_sign.svg",
          fit: BoxFit.cover, color: MyTheme.airGreenColor),
      "status": false,
    },
    {
      "icon": Icons.payment,
      "title": "confirm_your_offer_add_bank_account".tr,
      "status_icon": SvgPicture.asset("assets/images/svg/ico_ok_sign.svg",
          fit: BoxFit.cover, color: MyTheme.airGreenColor),
      "status": false,
    },
    {
      "icon": Icons.calendar_today_outlined,
      "title": "confirm_your_offer_add_date_of_birth".tr,
      "status_icon": SvgPicture.asset("assets/images/svg/ico_ok_sign.svg",
          fit: BoxFit.cover, color: MyTheme.airGreenColor),
      "status": false,
    },
    {
      "icon": Icons.calendar_view_day_outlined,
      "title": "confirm_your_offer_add_nid_number".tr,
      "status_icon": SvgPicture.asset("assets/images/svg/ico_ok_sign.svg",
          fit: BoxFit.cover, color: MyTheme.airGreenColor),
      "status": false,
    }
  ].obs;
}
