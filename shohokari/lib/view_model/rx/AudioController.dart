import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:get/get.dart';

class AudioController extends GetxController {
  var isAudio = false.obs;

  setAudio(bool isOn) async {
    isAudio.value = isOn;
    await PrefMgr.shared.setPrefBool("isAudio", isOn);
  }

  getAudio() async {
    final isAudio2 = await PrefMgr.shared.getPrefBool("isAudio");
    isAudio.value = isAudio2;
  }
}
