import 'dart:convert';
import 'dart:io';

import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/ResCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/timeline/TimelinePostModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/CommentsAdditionalAttributesModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/UserCommentPublicModelList.dart';
import 'package:aitl/view/dashboard/messages/comments/taskdetails_comments_page.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/picker/CamPicker.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/MyTaskController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum CommentsViewType {
  COMMENT_TYPE,
  REPLY_TYPE,
}

class TimeLineHelper {
  drawTimelineHeading() {
    return Txt(
      txt: "donot_share_personal_info_msg".tr, // textViewReplyComments
      txtColor: MyTheme.gray4Color,
      txtSize: MyTheme.txtSize - .2,
      txtAlign: TextAlign.start,
      isBold: false,
    );
  }

  drawMessageBox(
      {context,
      String picUrl,
      String hintText,
      Icon iconLeft,
      Icon iconRight,
      TextEditingController textController,
      Function(File, String) callbackCam}) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      color: Colors.transparent,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          (picUrl != null)
              ? CircleAvatar(
                  radius: 20,
                  backgroundColor: Colors.transparent,
                  backgroundImage: MyNetworkImage.loadProfileImage(picUrl),
                )
              : SizedBox(),
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: (iconLeft == null)
                ? Container(
                    decoration: new BoxDecoration(
                        border: Border.all(color: Colors.grey),
                        borderRadius:
                            new BorderRadius.all(const Radius.circular(50))),
                    child: IconButton(
                        iconSize: 20,
                        icon: Image.asset(
                            "assets/images/icons/attachment_icon.png"),
                        onPressed: () {
                          CamPicker().showCamDialog(
                            context: context,
                            isRear: false,
                            callback: (File path) {
                              if (path != null) {
                                callbackCam(path, textController.text);
                              }
                            },
                          );
                        }),
                  )
                : IconButton(
                    iconSize: 20,
                    icon: iconLeft,
                    onPressed: () {
                      CamPicker().showCamDialog(
                        context: context,
                        isRear: false,
                        callback: (File path) {
                          if (path != null) {
                            callbackCam(path, textController.text);
                          }
                        },
                      );
                    }),
          ),
          SizedBox(width: 10),
          Expanded(
            child: Container(
              decoration: new BoxDecoration(
                  color: Colors.transparent,
                  border: Border.all(color: Colors.grey),
                  borderRadius:
                      new BorderRadius.all(const Radius.circular(20))),
              child: Row(
                children: [
                  /*Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: IconButton(
                        iconSize: 30,
                        icon: Icon(
                          Icons.tag_faces,
                          color: Colors.black54,
                        ),
                        onPressed: () {}),
                  ),*/

                  Expanded(
                    child: TextField(
                      controller: textController,
                      textInputAction: TextInputAction.send,
                      keyboardType: TextInputType.multiline,
                      textAlign: TextAlign.start,
                      onSubmitted: (value) {
                        //onSendClicked(textController);
                        callbackCam(null, value);
                      },
                      maxLength: 255,
                      autocorrect: false,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 14,
                      ),
                      decoration: InputDecoration(
                        isDense: true,
                        //isCollapsed: true,
                        counter: Offstage(),
                        border: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        enabledBorder: InputBorder.none,
                        errorBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        hintStyle: TextStyle(color: MyTheme.gray4Color),
                        contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 5),
                        hintText: hintText,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          iconRight == null
              ? TextButton(
                  onPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    callbackCam(null, textController.text);
                  },
                  child: Text(
                    "send".tr,
                    style: TextStyle(color: MyTheme.brandColor, fontSize: 20),
                  ),
                )
              : Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Container(
                      decoration: new BoxDecoration(
                          color: MyTheme.dGreenColor,
                          borderRadius:
                              new BorderRadius.all(const Radius.circular(50))),
                      child: IconButton(
                          icon: iconRight,
                          onPressed: () {
                            callbackCam(null, textController.text);
                          })),
                ),
        ],
      ),
    );
  }

  drawQ(
      {context,
      MyTaskController myTaskController,
      List<TimelinePostModel> listTimelinePostsModel,
      bool isPoster,
      bool isReply = false,
      Function callback}) {
    if (listTimelinePostsModel == null || listTimelinePostsModel.length == 0)
      return SizedBox();
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Container(
        child: ListView.builder(
          shrinkWrap: true,
          primary: false,
          itemCount: listTimelinePostsModel.length,
          itemBuilder: (context, i) {
            var model = listTimelinePostsModel[i];
            List<UserCommentPublicModelList> modelUser =
                model.userCommentPublicModelList;
            if (modelUser.length > 0) {
              List<Widget> list = [];
              list.add(
                _drawQItem(
                  context: context,
                  map: {
                    "timelineId": model.id,
                    "url": model.ownerImageUrl,
                    "name": model.ownerName,
                    "msg": model.message,
                    "additional": model.additionalAttributeValue,
                    "userId": userData.userModel.id, //model.ownerId,
                    "taskUserId": myTaskController.getTaskModel().userId,
                    "ownerId": model.ownerId,
                    "dateCreated": model.dateCreated,
                    //"viewType":model.
                  },
                  isPublicUser: false,
                  isPoster: isPoster,
                  isReply: isReply,
                  callback: () {
                    callback();
                  },
                ),
              );
              for (UserCommentPublicModelList model2 in modelUser) {
                var user = model2.user;
                list.add(
                  _drawQItem(
                    context: context,
                    map: {
                      "timelineId": null,
                      "url": user.profileImageUrl,
                      "name": user.name,
                      "msg": model2.commentText,
                      "additional": model2.additionalData,
                      "userId": user.id, //user.id,
                      "taskUserId": myTaskController.getTaskModel().userId,
                      "ownerId": 0,
                      "dateCreated": model2.dateCreated,
                    },
                    isPublicUser: true,
                    isPoster: isPoster,
                    isReply: isReply,
                    callback: () {
                      callback();
                    },
                  ),
                );
              }
              //if (isPoster) {
              list.add(Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: UIHelper().drawLine()));
              //}
              return Column(
                children: list,
              );
            } else {
              return _drawQItem(
                context: context,
                map: {
                  "timelineId": model.id,
                  "url": model.ownerImageUrl,
                  "name": model.ownerName,
                  "msg": model.message,
                  "additional": model.additionalAttributeValue,
                  "userId": userData.userModel.id, //model.ownerId,
                  "taskUserId": myTaskController.getTaskModel().userId,
                  "ownerId": model.ownerId,
                  "dateCreated": model.dateCreated,
                },
                isPublicUser: false,
                isReply: isReply,
                isPoster: isPoster,
                callback: () {
                  callback();
                },
              );
            }
          },
        ),
      ),
    );
  }

  _drawQItem(
      {context,
      Map map,
      bool isPublicUser,
      bool isPoster,
      bool isReply,
      Function callback}) {
    double width = MediaQuery.of(context).size.width;
    String additional = "";
    String msg = "";
    try {
      if (isPublicUser) {
        msg = map['msg'];
        additional = map['additional'];
      } else {
        try {
          msg = map['msg'];
          if (map['additional'] != '') {
            //  "{"ResponseData":{"Images":[{"CanComment":false,"DateCreatedLocal":"0001-01-01T00:00:00","DateCreatedUtc":"2021-07-08T17:36:19.3463903Z","FullyLoaded":false,"LikeStatus":0,"Id":87677,"MediaType":0,"MimeType":"image/jpeg","NextMediaId":0,"ThumbnailUrl":"https://herotasker.com:443/api//media/uploadpictures/05636785261b4e87be4d374e3eed3726_1000_1000.jpg","TotalComments":0,"TotalLikes":0,"Url":"https://herotasker.com:443/api//media/uploadpictures/05636785261b4e87be4d374e3eed3726_0_0.jpg","attachmentType":"OTHER"}]},"Success":true}"
            final CommentsAdditionalAttributesModel
                commentsAdditionalAttributesModel =
                CommentsAdditionalAttributesModel.fromJson(
                    json.decode(map['additional']));
            additional =
                commentsAdditionalAttributesModel.responseData.images[0].url;
          }
        } catch (e) {}
      }
    } catch (e) {}
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Container(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            (isPublicUser)
                ? CircleAvatar(
                    radius: 30,
                    backgroundColor: Colors.transparent,
                  )
                : SizedBox(),
            CircleAvatar(
              radius: 30,
              backgroundColor: Colors.transparent,
              backgroundImage: MyNetworkImage.loadProfileImage(map['url']),
            ),
            SizedBox(width: 10),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Flexible(
                        flex: 6,
                        child: Txt(
                            txt: map['name'],
                            txtColor: MyTheme.gray4Color,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ),
                      SizedBox(width: 20),
                      (map['ownerId'] == map['taskUserId'] ||
                              map['taskUserId'] == map['userId'])
                          ? Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                color: MyTheme.gray5Color.withAlpha(120),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 8, right: 8, top: 2, bottom: 2),
                                child: Text(
                                  "poster".tr,
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 15,
                                  ),
                                ),
                              ),
                            )
                          : SizedBox(),
                    ],
                  ),
                  SizedBox(height: 5),
                  Txt(
                      txt: msg ?? '',
                      txtColor: MyTheme.gray5Color,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                  additional != ""
                      ? GestureDetector(
                          onTap: () {
                            Get.to(() => PicFullView(
                                  title: "full_screen_image_screen_title".tr,
                                  url: additional,
                                ));
                          },
                          child: Container(
                            width: width * AppConfig.chatScrollHeight,
                            height: width * AppConfig.chatScrollHeight,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                image:
                                    MyNetworkImage.loadProfileImage(additional),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        )
                      : SizedBox(),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Flexible(
                        flex: 2,
                        child: Txt(
                            txt: DateFun.getTimeAgoTxt(map['dateCreated']),
                            txtColor: MyTheme.gray4Color,
                            txtSize: MyTheme.txtSize,
                            txtAlign: TextAlign.start,
                            isBold: false),
                      ),
                      (!isPublicUser && isReply)
                          ? TextButton.icon(
                              icon: Icon(
                                Icons.reply,
                                color: Colors.grey,
                              ),
                              onPressed: () {
                                Get.to(() => TaskDetailsCommentsPage(
                                      timelineId: map['timelineId'],
                                    )).then((value) => callback);
                              },
                              label: Text(
                                "reply".tr,
                                style: TextStyle(
                                  color: Colors.grey,
                                  fontSize: 14,
                                ),
                              ),
                            )
                          : SizedBox(),
                      Expanded(child: SizedBox()),
                      GestureDetector(
                        onTapDown: (TapDownDetails details) {
                          UIHelper().onReportClicked(
                            context,
                            details.globalPosition,
                            map['userId'],
                            ResCfg.RESOULUTION_TYPE_TASK_COMMENT_SPAMREPORT,
                          );
                        },
                        child: Icon(
                          Icons.more_horiz,
                          color: MyTheme.gray3Color,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
