import 'dart:convert';

import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';

class TaskData {
  static final TaskData _appData = new TaskData._internal();

  TaskModel taskModel;
  List<Map<String, dynamic>> listTaskImages;

  factory TaskData() {
    return _appData;
  }
  TaskData._internal();

  setTask() async {
    try {} catch (e) {}
  }

  destroyTask() {
    //  task data deallocate
    taskModel = null;
    listTaskImages = null;
  }
}

final taskData = TaskData();
