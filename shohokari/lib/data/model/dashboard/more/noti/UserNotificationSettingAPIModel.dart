import 'UserNotificationSettingModel.dart';

class UserNotificationSettingAPIModel {
  bool success;
  ResponseData responseData;

  UserNotificationSettingAPIModel({this.success, this.responseData});

  UserNotificationSettingAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  UserNotificationSettingModel userNotificationSetting;
  ResponseData({this.userNotificationSetting});

  ResponseData.fromJson(Map<String, dynamic> json) {
    userNotificationSetting = json['UserNotificationSetting'] != null
        ? new UserNotificationSettingModel.fromJson(
            json['UserNotificationSetting'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userNotificationSetting != null) {
      data['UserNotificationSetting'] = this.userNotificationSetting.toJson();
    }
    return data;
  }
}
