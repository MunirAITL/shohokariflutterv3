import 'package:aitl/data/model/dashboard/more/profile/AboutModel.dart';

class AboutAPIModel {
  bool success;
  ResponseData responseData;

  AboutAPIModel({this.success, this.responseData});

  AboutAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  AboutModel about;
  ResponseData({this.about});
  ResponseData.fromJson(Map<String, dynamic> json) {
    about =
        json['About'] != null ? new AboutModel.fromJson(json['About']) : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.about != null) {
      data['About'] = this.about.toJson();
    }
    return data;
  }
}
