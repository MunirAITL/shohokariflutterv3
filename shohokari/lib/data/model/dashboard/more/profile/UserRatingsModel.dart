import 'package:aitl/data/model/dashboard/user/PublicUserModel.dart';

class UserRatingsModel {
  User user;
  String comments;
  String creationDate;
  int employeeId;
  String employeeName;
  String initiatorName;
  bool isPoster;
  String profileImageUrl;
  double rating;
  int status;
  int taskBiddingId;
  String taskTitle;
  int userId;
  int id;
  String taskTitleUrl;
  String ownerProfileUrl;

  UserRatingsModel({
    this.user,
    this.comments,
    this.creationDate,
    this.employeeId,
    this.employeeName,
    this.initiatorName,
    this.isPoster,
    this.profileImageUrl,
    this.rating,
    this.status,
    this.taskBiddingId,
    this.taskTitle,
    this.userId,
    this.id,
    this.taskTitleUrl,
    this.ownerProfileUrl,
  });

  UserRatingsModel.fromJson(Map<String, dynamic> json) {
    user = json['User'] != null ? new User.fromJson(json['User']) : null;
    comments = json['Comments'] ?? '';
    creationDate = json['CreationDate'] ?? '';
    employeeId = json['EmployeeId'] ?? 0;
    employeeName = json['EmployeeName'] ?? '';
    initiatorName = json['InitiatorName'] ?? '';
    isPoster = json['IsPoster'] ?? false;
    profileImageUrl = json['ProfileImageUrl'] ?? '';
    rating = json['Rating'] ?? 0.0;
    status = json['Status'] ?? 0;
    taskBiddingId = json['TaskBiddingId'] ?? 0;
    taskTitle = json['TaskTitle'] ?? '';
    userId = json['UserId'] ?? 0;
    id = json['Id'] ?? 0;
    taskTitleUrl = json['TaskTitleUrl'] ?? '';
    ownerProfileUrl = json['OwnerProfileUrl'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    data['Comments'] = this.comments;
    data['CreationDate'] = this.creationDate;
    data['EmployeeId'] = this.employeeId;
    data['EmployeeName'] = this.employeeName;
    data['InitiatorName'] = this.initiatorName;
    data['IsPoster'] = this.isPoster;
    data['ProfileImageUrl'] = this.profileImageUrl;
    data['Rating'] = this.rating;
    data['Status'] = this.status;
    data['TaskBiddingId'] = this.taskBiddingId;
    data['TaskTitle'] = this.taskTitle;
    data['UserId'] = this.userId;
    data['Id'] = this.id;
    data['TaskTitleUrl'] = this.taskTitleUrl;
    data['OwnerProfileUrl'] = this.ownerProfileUrl;
    if (this.user != null) {
      data['User'] = this.user.toJson();
    }
    return data;
  }
}
