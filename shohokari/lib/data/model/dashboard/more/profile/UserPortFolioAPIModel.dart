import 'UserPortfulioModel.dart';

class UserPortFolioAPIModel {
  bool success;
  ResponseData responseData;

  UserPortFolioAPIModel({this.success, this.responseData});

  UserPortFolioAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  UserPortfulioModel userPortfulio;
  ResponseData({this.userPortfulio});
  ResponseData.fromJson(Map<String, dynamic> json) {
    userPortfulio = json['UserPortfulio'] != null
        ? new UserPortfulioModel.fromJson(json['UserPortfulio'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.userPortfulio != null) {
      data['UserPortfulio'] = this.userPortfulio.toJson();
    }
    return data;
  }
}
