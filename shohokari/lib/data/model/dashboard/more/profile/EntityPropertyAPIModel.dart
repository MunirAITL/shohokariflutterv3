import 'EntityPropertyModel.dart';

class EntityPropertyAPIModel {
  bool success;
  ResponseData responseData;
  EntityPropertyAPIModel({this.success, this.responseData});
  EntityPropertyAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  EntityPropertyModel entityProperty;
  ResponseData({this.entityProperty});
  ResponseData.fromJson(Map<String, dynamic> json) {
    entityProperty = json['EntityProperty'] != null
        ? new EntityPropertyModel.fromJson(json['EntityProperty'])
        : null;
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.entityProperty != null) {
      data['EntityProperty'] = this.entityProperty.toJson();
    }
    return data;
  }
}
