class UserPortfulioModel {
  int id;
  int userId;
  String user;
  String documentUrl;
  String portfulioItems;
  UserPortfulioModel({
    this.id,
    this.userId,
    this.user,
    this.documentUrl,
    this.portfulioItems,
  });
  UserPortfulioModel.fromJson(Map<String, dynamic> json) {
    id = json['Id'] ?? 0;
    userId = json['UserId'] ?? 0;
    user = json['User'] ?? '';
    documentUrl = json['DocumentUrl'] ?? '';
    portfulioItems = json['PortfulioItems'] ?? '';
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Id'] = this.id;
    data['UserId'] = this.userId;
    data['User'] = this.user;
    data['DocumentUrl'] = this.documentUrl;
    data['PortfulioItems'] = this.portfulioItems;
    return data;
  }
}
