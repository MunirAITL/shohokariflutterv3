import 'PaymentMethodsModel.dart';

class PaymentMethodAPIModel {
  bool success;
  ResponseData responseData;
  PaymentMethodAPIModel({this.success, this.responseData});

  PaymentMethodAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'];
    responseData = json['ResponseData'] != null
        ? new ResponseData.fromJson(json['ResponseData'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['Success'] = this.success;
    if (this.responseData != null) {
      data['ResponseData'] = this.responseData.toJson();
    }
    return data;
  }
}

class ResponseData {
  PaymentMethodsModel paymentMethod;
  ResponseData({this.paymentMethod});

  ResponseData.fromJson(Map<String, dynamic> json) {
    paymentMethod = json['PaymentMethod'] != null
        ? new PaymentMethodsModel.fromJson(json['PaymentMethod'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.paymentMethod != null) {
      data['PaymentMethod'] = this.paymentMethod.toJson();
    }
    return data;
  }
}
