class SavePicAPIModel {
  bool success;
  dynamic responseData;
  SavePicAPIModel.fromJson(Map<String, dynamic> json) {
    success = json['Success'] ?? true;
    responseData = json['ResponseData'] ?? null;
  }
}
