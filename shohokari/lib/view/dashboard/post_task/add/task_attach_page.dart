import 'dart:io';

import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/DelPicAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/GetPicModel.dart';
import 'package:aitl/data/model/dashboard/posttask/taskpic/SavePicAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/picker/CamPicker.dart';
import 'package:aitl/view/widgets/picker/PickerModel.dart';
import 'package:aitl/view/widgets/views/pic_view.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'addtask_base.dart';

class TaskAttachmentScreen extends StatefulWidget {
  final UserModel userModel;
  const TaskAttachmentScreen({Key key, @required this.userModel})
      : super(key: key);
  @override
  State createState() => _TaskAttachmentScreenState();
}

class _TaskAttachmentScreenState
    extends BaseAddTaskStatefull<TaskAttachmentScreen>
    with APIStateListener, StateListener {
//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(ObserverState state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value) AudioMgr().play("task_attachment");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  void onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final MediaUploadFilesModel mediaModel =
                (model as MediaUploadFilesAPIModel).responseData.images[0];
            if (taskController.getTaskModel().taskImages == null)
              taskController.getTaskModel().taskImages = [];
            await APIViewModel().req<SavePicAPIModel>(
              context: context,
              url: APIPostTaskCfg.SAVE_PIC_URL,
              reqType: ReqType.Post,
              param: {
                "EntityId": taskController.getTaskModel().id,
                "EntityName": "Task",
                "PropertyName": "Task",
                "Value": mediaModel.id,
              },
              callback: (model2) {
                if (model2 != null && mounted) {
                  if (model2.success) {
                    taskController.getTaskModel().taskImages.add({
                      'isGetPicModel': false,
                      'id': mediaModel.id,
                      'url': mediaModel.url
                    });
                    setState(() {});
                  }
                }
              },
            );
          }
        }
      } else if (apiState.type == APIType.get_pic &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            final List<GetPicModel> listPicModel =
                (model as GetPicAPIModel).responseData.taskPictures;
            taskController.getTaskModel().taskImages = [];
            for (GetPicModel picModel in listPicModel) {
              try {
                taskController.getTaskModel().taskImages.add({
                  'isGetPicModel': true,
                  'id': picModel.id,
                  'url': picModel.thumbnailPath,
                });
              } catch (e) {
                myLog(e.toString());
              }
            }
            print(taskController.getTaskModel().taskImages.toString());
            setState(() {});
          }
        }
      } else if (apiState.type == APIType.del_pic &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            print(taskController.getTaskModel().taskImages.length.toString());
            taskController.getTaskModel().taskImages.remove(apiState.data);
            print(taskController.getTaskModel().taskImages.length.toString());
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  uploadTaskImages(int pageNo) {}

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }

    try {
      NetworkMgr().dispose();
    } catch (e) {}
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
      StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    } catch (e) {}
    try {
      if (taskController.getTaskModel().id != null) {
        await APIViewModel().req<GetPicAPIModel>(
          context: context,
          apiState: APIState(APIType.get_pic, this.runtimeType, null),
          reqType: ReqType.Get,
          url: APIPostTaskCfg.GET_PIC_URL.replaceAll(
            "#taskId#",
            taskController.getTaskModel().id.toString(),
          ),
        );
      }
    } catch (e) {}
  }

  onNextClicked() {}
  onDelTaskClicked() {}

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          elevation: MyTheme.appbarElevation,
          title: UIHelper().drawAppbarTitle(title: 'task_attachments'.tr),
          centerTitle: false,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    //myLog(taskController.taskImages.value);

    int len = 1;
    try {
      len = (taskController.getTaskModel().taskImages.length <
              AppConfig.totalUploadLimit)
          ? taskController.getTaskModel().taskImages.length + 1
          : taskController.getTaskModel().taskImages.length;
    } catch (e) {}
    return Container(
      child: GridView.count(
        crossAxisCount: 3,
        crossAxisSpacing: 10,
        mainAxisSpacing: 10,
        padding: EdgeInsets.all(10.0),
        children: List.generate(
          len,
          (index) {
            String url = "";
            int id = 0;
            var model;
            if (taskController.getTaskModel().taskImages != null) {
              if (index < taskController.getTaskModel().taskImages.length) {
                model = taskController.getTaskModel().taskImages[index];
                id = model['id'];
                url = model['url'];
              } else
                model = null;
            } else
              model = null;

            return Card(
              child: (model != null)
                  ? Stack(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: MyNetworkImage.loadProfileImage(url),
                              fit: BoxFit.cover,
                            ),
                            shape: BoxShape.rectangle,
                          ),
                        ),
                        Positioned(
                          right: -5,
                          top: -5,
                          child: Container(
                            width: 30,
                            height: 30,
                            child: MaterialButton(
                              onPressed: () {
                                PickerModel().show(
                                    context: context,
                                    title: "chooseopt".tr,
                                    listBtn: [
                                      MaterialButton(
                                        child: Text("cancel".tr),
                                        onPressed: () {
                                          Navigator.pop(context, 0);
                                        },
                                      ),
                                      MaterialButton(
                                        child: Text("view".tr),
                                        onPressed: () {
                                          Navigator.pop(context, 0);
                                          Get.to(() => PicFullView(
                                              url: url,
                                              title:
                                                  "full_screen_image_screen_title"
                                                      .tr));
                                        },
                                      ),
                                      MaterialButton(
                                        child: Text("delete".tr),
                                        onPressed: () async {
                                          Navigator.pop(context, 0);
                                          confirmDialog(
                                            context: context,
                                            title: "confirmation".tr,
                                            msg: "confirm_del_task_attachment"
                                                .tr,
                                            callbackYes: () async {
                                              await APIViewModel()
                                                  .req<DelPicAPIModel>(
                                                context: context,
                                                apiState: APIState(
                                                    APIType.del_pic,
                                                    this.runtimeType,
                                                    model),
                                                url: APIPostTaskCfg.DEL_PIC_URL
                                                    .replaceAll("#mediaId#",
                                                        id.toString()),
                                                reqType: ReqType.Delete,
                                              );
                                            },
                                          );
                                        },
                                      ),
                                    ]);
                              },
                              color: MyTheme.gray1Color,
                              child: Icon(Icons.close, color: Colors.black),
                              padding: EdgeInsets.all(0),
                              shape: CircleBorder(),
                            ),
                          ),
                        ),
                      ],
                    )
                  : GestureDetector(
                      onTap: () {
                        CamPicker().showCamDialog(
                          context: context,
                          isRear: false,
                          callback: (File path) {
                            if (path != null) {
                              APIViewModel().upload(
                                context: context,
                                apiState: APIState(APIType.media_upload_file,
                                    this.runtimeType, null),
                                file: path,
                              );
                            }
                          },
                        );
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: MyTheme.gray1Color,
                        ),
                        child: Icon(
                          Icons.add,
                          size: 100,
                          color: MyTheme.gray3Color,
                        ),
                      ),
                    ),
            );
          },
        ),
      ),
    );
  }
}
