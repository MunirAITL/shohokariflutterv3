import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/post_task/add/task_attach_page.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/stepbar/StepBar.dart';
import 'package:aitl/view/widgets/txt/AppbarText..dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/TaskController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BaseAddTaskStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final taskController = Get.put(TaskController());

  BaseAddTaskStatefull() {
    // Parent constructor
  }

  //  abstract methods
  onNextClicked();
  onDelTaskClicked();
  uploadTaskImages(int pageNo);

  drawAppbar({
    @required int pageNo,
    @required userModel,
    @required title,
    @required int pos,
    @required isBold1,
    @required isBold2,
    @required isBold3,
  }) {
    final totalDots = getW(context) / 20;
    return AppBar(
      elevation: MyTheme.appbarElevation,
      backgroundColor: MyTheme.bgColor,
      iconTheme: MyTheme.themeData.iconTheme,
      leading: IconButton(
        icon: Icon(Icons.arrow_back),
        onPressed: () async {
          if (pageNo == 1) {
            await uploadTaskImages(pageNo);
          }
          Get.back(result: pageNo);
        },
      ),
      title: UIHelper().drawAppbarTitle(title: 'post_task_screen_title_new'.tr),
      centerTitle: false,
      actions: [
        taskController.getTaskModel().id != null
            ? IconButton(
                icon: Icon(Icons.attach_file),
                onPressed: () {
                  Get.to(() => TaskAttachmentScreen(
                        userModel: userModel,
                      ));
                },
              )
            : SizedBox(),
        taskController.getTaskModel().id != null
            ? IconButton(
                icon: Icon(Icons.delete),
                onPressed: () async {
                  await audioController.getAudio();
                  if (audioController.isAudio.value)
                    AudioMgr().play("delete_task_prompt");
                  confirmDialog(
                    context: context,
                    title: null,
                    msg: "delete_task_confirm".tr,
                    callbackYes: () {
                      onDelTaskClicked();
                    },
                  );
                },
              )
            : SizedBox(),
        drawAppbarText(
          txt: "next_btn".tr,
          callback: () {
            onNextClicked();
          },
        ),
        SizedBox(width: 10),
      ],
      /*drawSoundLngBox(this.runtimeType, audioController, [
        taskController.getTaskModel().id != null
            ? IconButton(
                icon: Icon(Icons.attach_file),
                onPressed: () {
                  Get.to(() => TaskAttachmentScreen(
                        userModel: userModel,
                      ));
                },
              )
            : SizedBox(),
        taskController.getTaskModel().id != null
            ? IconButton(
                icon: Icon(Icons.delete),
                onPressed: () {
                  Get.dialog(
                    ConfirmDialog(
                        callback: () {
                          onDelTaskClicked();
                        },
                        title: null,
                        msg: "delete_task_confirm".tr),
                  );
                },
              )
            : SizedBox(),
        drawAppbarText(
          txt: "next_btn".tr,
          callback: () {
            onNextClicked();
          },
        ),
        SizedBox(width: 10),
      ]),*/
      bottom: PreferredSize(
          child: Container(
            color: Colors.white,
            height: getHP(context, 8),
            width: getW(context),
            child: ListView(
              scrollDirection: Axis.horizontal,
              primary: false,
              padding: const EdgeInsets.only(left: 20, right: 20),
              children: [
                drawStepBar(
                  context: context,
                  isSelected: (pos == 0) ? true : false,
                  pos: 1,
                  txt: "post_task_segment_title_details".tr,
                  isBold: isBold1,
                  dotsLen: 0,
                ),
                drawStepBar(
                  context: context,
                  isSelected: (pos == 1) ? true : false,
                  pos: 2,
                  txt: "post_task_segment_title_date".tr,
                  isDots: true,
                  isBold: isBold2,
                  dotsLen: totalDots,
                ),
                drawStepBar(
                  context: context,
                  isSelected: (pos == 2) ? true : false,
                  pos: 3,
                  txt: "post_task_segment_title_price".tr,
                  isDots: true,
                  isBold: isBold3,
                  dotsLen: totalDots,
                ),
              ],
            ),
          ),
          preferredSize: Size.fromHeight(getHP(context, 8))),
    );
  }
}
