import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/model/dashboard/posttask/taskform/UserTaskItemFormsModel.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/checkbox/CustomCheckbox.dart';
import 'package:aitl/view/widgets/datepicker/DatePickerView.dart';
import 'package:aitl/view/widgets/google/GPlacesView.dart';
import 'package:aitl/view/widgets/stepbar/StepperNum.dart';
import 'package:aitl/view/widgets/input/InputBox.dart';
import 'package:aitl/view/widgets/input/InputTitleBoxHT.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/rx/OptSelController.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geocoding.dart';
import 'package:intl/intl.dart';
import '../../dashboard_base.dart';

abstract class BaseAddTaskFormStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final optController = Get.put(OptSelController());

  final estBudget = TextEditingController();
  final cmt = TextEditingController();
  final focusEstBudget = FocusNode();
  final focusCmt = FocusNode();

  List<UserTaskItemFormsModel> listTaskItemFormModel = [];

  double currentPage = 0;
  PageController controller = PageController(initialPage: 0);
  //int pageCount = 8;

  Map<int, TextEditingController> inputOthers = {};

  //List<GroupModel> listRB = [];

  Map<int, bool> isOthers = {};

  //  page3
  int totalTaskers = 1;

  //  page4
  String dueDate = "";

  //  page5
  String taskAddress = "search".tr;
  Location taskCord;

  onGetStarted();

  //  **********************************************  GENERIC CHECKBOX

  drawCheckBox(UserTaskItemFormsModel model, int index, int order) {
    if (listTaskItemFormModel.length == 0 || !mounted) return SizedBox();

    var answers = "";
    if (isLng(AppDefine.GEO_CODE)) {
      answers = model.answersBn;
    } else {
      answers = model.answers;
    }
    final arr = answers.split(',');

    if (optController.listCB[order] == null) {
      List<Map<int, bool>> m = [];
      for (int i = 0; i < arr.length; i++) {
        m.add({i: false});
        if (arr[i] == "others".tr) {
          inputOthers[index] = TextEditingController();
        }
      }
      optController.listCB[order] = m;
    }

    var o = optController.listCB[order];

    var question = "";
    if (isLng(AppDefine.GEO_CODE)) {
      question = model.questionBn;
    } else {
      question = model.question;
    }

    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(20),
                child: Txt(
                    txt: question,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              SizedBox(height: 20),
              _drawCB(o, arr, index, order),
            ],
          ),
        ),
      ),
    );
  }

  _drawCB(List<Map<int, bool>> o, List arr, int index, int order) {
    List<Widget> list = [];
    for (int i = 0; i < arr.length; i++) {
      final map = o[i].obs;
      list.add(Obx(() => Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomCheckbox(
                txt: arr[i],
                isSelected: map[i],
                themeData: MyTheme.checkboxThemeData,
                callback: (v) {
                  if (arr[i].toString() == "others".tr) {
                    isOthers[index] = v;
                  }
                  map[i] = v;
                  o[i] = map;
                },
              ),
              (map[i] && arr[i].toString() == "others".tr)
                  ? Padding(
                      padding: const EdgeInsets.only(left: 50),
                      child: InputBox(
                        ctrl: inputOthers[index],
                        lableTxt: "others".tr,
                        kbType: TextInputType.text,
                        len: 50,
                        isPwd: false,
                      ),
                    )
                  : SizedBox(),
            ],
          )));
    }
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: list);
  }

  //  **********************************************  GENERIC RADIO BUTTON

  drawRadioBox(UserTaskItemFormsModel model, int index, int order) {
    if (listTaskItemFormModel.length == 0) return SizedBox();
    var answers = "";
    if (isLng(AppDefine.GEO_CODE)) {
      answers = model.answersBn;
    } else {
      answers = model.answers;
    }
    final arr = answers.split(',');
    if (optController.listRB[order] == null) {
      for (int i = 0; i < arr.length; i++) {
        if (arr[i] == "others".tr) {
          inputOthers[index] = TextEditingController();
        }
      }
      optController.listRB[order] = 0;
    }

    var g = optController.listRB[order].obs;

    var question = "";
    if (isLng(AppDefine.GEO_CODE)) {
      question = model.questionBn;
    } else {
      question = model.question;
    }

    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(20),
                child: Txt(
                    txt: question,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              SizedBox(height: 20),
              _drawRB(g, arr, index, order),
            ],
          ),
        ),
      ),
    );
  }

  _drawRB(RxInt g, List arr, int index, int order) {
    List<Widget> list = [];
    for (int i = 0; i < arr.length; i++) {
      //final mapGroup = g[i].obs;

      //print(map[index]);
      list.add(Obx(() => Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Theme(
                data: MyTheme.radioBlackThemeData,
                child: RadioListTile(
                  controlAffinity: ListTileControlAffinity.leading,
                  dense: true,
                  value: i,
                  groupValue: g.value,
                  //selected: mapGroup.value.selected,
                  //activeColor: Colors.purple,
                  onChanged: (int val) {
                    //listRBIndex = val;
                    //mapGroup.value.index = val;
                    //mapGroup.value.selected = true;
                    g.value = val;
                    optController.listRB[order] = g.value;
                    if (arr[i].toString() == "others".tr) {
                      isOthers[index] = true;
                    } else {
                      isOthers[index] = false;
                    }
                    //setState(() {});
                    //});
                  },
                  title: Txt(
                    txt: arr[i],
                    txtColor: Colors.black,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false,
                  ),
                ),
              ),
              (isOthers[index] != null)
                  ? (isOthers[index] && arr[i].toString() == "others".tr)
                      ? Padding(
                          padding: const EdgeInsets.only(left: 50),
                          child: InputBox(
                            ctrl: inputOthers[index],
                            lableTxt: "others".tr,
                            kbType: TextInputType.text,
                            len: 50,
                            isPwd: false,
                          ),
                        )
                      : SizedBox()
                  : SizedBox(),
            ],
          )));
    }
    return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: list);
  }

  drawWorkerNumberBox(UserTaskItemFormsModel model) {
    if (listTaskItemFormModel.length == 0) return SizedBox();

    var question = "";
    if (isLng(AppDefine.GEO_CODE)) {
      question = model.questionBn;
    } else {
      question = model.question;
    }

    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(20),
                child: Txt(
                    txt: question,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .5,
                    txtAlign: TextAlign.start,
                    isBold: true),
              ),
              SizedBox(height: 20),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Flexible(
                    flex: 6,
                    child: Txt(
                        txt: question,
                        txtColor: MyTheme.gray5Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                  ),
                  SizedBox(width: 10),
                  Flexible(
                    //flex: 2,
                    child: Txt(
                        txt: totalTaskers.toString(),
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize + .3,
                        txtAlign: TextAlign.center,
                        isBold: false),
                  ),
                  SizedBox(width: 10),
                  Flexible(
                    flex: 3,
                    child: StepperNum(
                      count: totalTaskers,
                      callback: (val) {
                        totalTaskers = val;
                        setState(() {});
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawDateTimeBox(UserTaskItemFormsModel model) {
    if (listTaskItemFormModel.length == 0) return SizedBox();

    var question = "";
    if (isLng(AppDefine.GEO_CODE)) {
      question = model.questionBn;
    } else {
      question = model.question;
    }

    final DateTime dateNow = DateTime.now();
    final dateExplast = DateTime(dateNow.year + 1, dateNow.month, dateNow.day);
    final dateExpfirst = DateTime(dateNow.year, dateNow.month, dateNow.day);
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(20),
                child: Txt(
                    txt: question,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              Padding(
                padding: const EdgeInsets.all(15),
                child: DatePickerView(
                  cap: null,
                  isCapBold: false,
                  capTxtColor: MyTheme.gray5Color,
                  txtColor: Colors.black,
                  topbotHeight: 2,
                  borderWidth: 0,
                  dt: (dueDate == '')
                      ? 'propose_new_time_due_date_label'.tr
                      : dueDate,
                  initialDate: dateExpfirst,
                  firstDate: dateExpfirst,
                  lastDate: dateExplast,
                  callback: (value) {
                    if (mounted) {
                      setState(() {
                        try {
                          dueDate = DateFormat('dd-MMM-yyyy')
                              .format(value)
                              .toString();
                        } catch (e) {
                          myLog(e.toString());
                        }
                      });
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawLocationBox(UserTaskItemFormsModel model) {
    if (listTaskItemFormModel.length == 0) return SizedBox();
    var question = "";
    if (isLng(AppDefine.GEO_CODE)) {
      question = model.questionBn;
    } else {
      question = model.question;
    }
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(20),
                child: Txt(
                    txt: question,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              GPlacesView(
                title: null,
                txtColor: MyTheme.gray5Color,
                isTxtBold: false,
                address: taskAddress,
                bgColor: Colors.white,
                titlePadding: 0,
                callback: (String _address, Location _loc) {
                  //callback(address);
                  FocusScope.of(context).requestFocus(new FocusNode());
                  taskAddress = _address;
                  taskCord = _loc;
                  setState(() {});
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawBudgetBox(UserTaskItemFormsModel model) {
    if (listTaskItemFormModel.length == 0) return SizedBox();
    var question = "";
    if (isLng(AppDefine.GEO_CODE)) {
      question = model.questionBn;
    } else {
      question = model.question;
    }
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(20),
                child: Txt(
                    txt: question,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15, right: 15),
                child: InputTitleBoxHT(
                  title: "",
                  ph: "00.00",
                  input: estBudget,
                  kbType: TextInputType.number,
                  inputAction: TextInputAction.next,
                  focusNode: focusEstBudget,
                  focusNodeNext: focusCmt,
                  len: 7,
                  minLen: 0,
                  prefixIco: Text(
                    getCurSign(),
                    style: TextStyle(color: Colors.black, fontSize: 25),
                  ),
                  onChange: (v) {},
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawTextArea(UserTaskItemFormsModel model) {
    if (listTaskItemFormModel.length == 0) return SizedBox();
    var question = "";
    if (isLng(AppDefine.GEO_CODE)) {
      question = model.questionBn;
    } else {
      question = model.question;
    }
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.all(20),
                child: Txt(
                    txt: question,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              Container(
                margin: const EdgeInsets.only(left: 20.0, right: 20, top: 10),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.grey),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: TextField(
                  controller: cmt,
                  focusNode: focusCmt,
                  minLines: 5,
                  maxLines: 10,
                  //expands: true,
                  autocorrect: false,
                  maxLength: 500,
                  keyboardType: TextInputType.multiline,
                  textInputAction: TextInputAction.done,
                  onEditingComplete: () {
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  style: TextStyle(
                    color: Colors.black,
                    fontSize:
                        getTxtSize(context: context, txtSize: MyTheme.txtSize),
                  ),
                  decoration: InputDecoration(
                    counter: Offstage(),
                    hintText: 'others'.tr,
                    hintStyle: TextStyle(color: Colors.grey),
                    //labelText: 'Your message',
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                    contentPadding: EdgeInsets.only(
                        left: 15, bottom: 11, top: 11, right: 15),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  drawBottomBar() {
    if (listTaskItemFormModel.length == 0) return SizedBox();
    final len = listTaskItemFormModel.length;
    return BottomAppBar(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            DotsIndicator(
              dotsCount: len,
              position: currentPage,
              decorator: DotsDecorator(
                color: Colors.black12, // Inactive color
                activeColor: MyTheme.bgColor,
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MMBtn(
                    txt: (currentPage == 0) ? "back".tr : "previous".tr,
                    height: getHP(context, MyTheme.btnHpa - 1),
                    width: getWP(context, 42),
                    radius: 20,
                    callback: () {
                      if (currentPage == 0)
                        Get.back();
                      else {
                        controller.previousPage(
                            duration: Duration(milliseconds: 100),
                            curve: Curves.decelerate);
                      }
                    }),
                MMBtn(
                    txt: (currentPage == len - 1)
                        ? "on_boarding_poster_four_continue_button_label".tr
                        : "next_btn".tr,
                    height: getHP(context, MyTheme.btnHpa - 1),
                    width: getWP(context, 42),
                    radius: 20,
                    callback: () {
                      if ((currentPage == len - 1)) {
                        onGetStarted();
                      } else {
                        //if (currentPage != 1)
                        controller.nextPage(
                            duration: Duration(milliseconds: 100),
                            curve: Curves.decelerate);
                        //else
                        //onGetStarted();
                      }
                    }),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
