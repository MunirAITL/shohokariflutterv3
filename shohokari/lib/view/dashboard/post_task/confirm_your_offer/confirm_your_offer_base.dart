import 'dart:io';

import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/db/DBMgr.dart';
import 'package:aitl/data/model/auth/profile/RegProfileAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/EntityPropertyAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserBadgeAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/more/payment/payment_methods/receive_payment/receive_payment_page.dart';
import 'package:aitl/view/widgets/picker/CamPicker.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';

abstract class BaseConfirmYourOfferStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  uploadProfilePicClicked(cls) {
    CamPicker().showCamDialog(
      context: context,
      isRear: false,
      callback: (File path) {
        if (path != null) {
          APIViewModel().upload(
              context: context,
              file: path,
              callback: (model2) async {
                if (model2 != null && mounted) {
                  if (model2.success) {
                    final MediaUploadFilesModel model3 =
                        model2.responseData.images[0];
                    await APIViewModel().req<EntityPropertyAPIModel>(
                        context: context,
                        url: APIProfileCFg.ENTITY_PROPERTY_POST_URL,
                        reqType: ReqType.Post,
                        param: {
                          "EntityId": userData.userModel.id,
                          "EntityName": "User",
                          "PropertyName": "DefaultPictureId",
                          "Value": model3.id,
                        },
                        callback: (model4) {
                          if (model4 != null && mounted) {
                            if (model4.success) {
                              wsUserProfile(
                                  userId: userData.userModel.id,
                                  callback: (model5) async {
                                    try {
                                      await DBMgr.shared.setUserProfile(
                                          user: model5.responseData.user);
                                      await userData.setUserModel();
                                      showToast(
                                        context: context,
                                        msg:
                                            "Profile picture uploaded successfully",
                                      );
                                      setState(() {});
                                    } catch (e) {
                                      myLog(e.toString());
                                    }
                                  });
                            }
                          }
                        });
                  }
                }
              });
        }
      },
    );
  }

  paymentInfoClicked(cls) {
    Get.to(() => ReceivePaymentPage(isHideAppbar: false)).then((value) {
      setState(() {});
    });
  }

  dobClicked(cls) {
    final dateNow = DateTime.now();
    showDatePicker(
      context: context,
      initialDate: DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
      firstDate: DateTime(dateNow.year - 100, dateNow.month, dateNow.day),
      lastDate: DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
      builder: (context, child) {
        return Theme(
          data: ThemeData.light().copyWith(
            colorScheme: ColorScheme.light(primary: MyTheme.redColor),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ), // This will change to light theme.
          child: child,
        );
      },
    ).then((value) {
      if (value != null) {
        try {
          final dueDate =
              DateFormat(DateFun.getDateBDFormat()).format(value).toString();
          APIViewModel().req<RegProfileAPIModel>(
              context: context,
              url: APIAuthCfg.REG_PROFILE_PUT_URL,
              reqType: ReqType.Put,
              param: {
                "Address": userData.userModel.address,
                "BriefBio": userData.userModel.briefBio,
                "CommunityId": userData.communityId,
                "DateofBirth": dueDate,
                "Email": userData.userModel.email,
                "FirstName": userData.userModel.firstName,
                //"Cohort": '',
                "Headline": userData.userModel.headline,
                "Id": userData.userModel.id,
                "LastName": userData.userModel.lastName,
                //"Latitude": ,
                //"Longitude": loc.lng,
                "MobileNumber": userData.userModel.mobileNumber,
              },
              callback: (model) {
                if (model != null && mounted) {
                  if (model.success) {}
                }
              });
        } catch (e) {
          myLog(e.toString());
        }
      }
    });
  }

  uploadNIDClicked(cls) {
    CamPicker().showCamDialog(
      context: context,
      isRear: false,
      callback: (File path) {
        if (path != null) {
          APIViewModel().upload(
              context: context,
              file: path,
              callback: (model2) async {
                if (model2 != null && mounted) {
                  if (model2.success) {
                    final MediaUploadFilesModel model3 =
                        model2.responseData.images[0];
                    await APIViewModel().req<UserBadgeAPIModel>(
                        context: context,
                        url: APIProfileCFg.USER_BADGE_POST_URL,
                        reqType: ReqType.Post,
                        param: {
                          "UserId": userData.userModel.id,
                          "Status": TaskStatusCfg.TASK_STATUS_ACTIVE,
                          "Title": "NationalIDCard",
                          "Type": "NationalIDCard",
                          "ReferenceType": "NationalIDCard",
                          "IsVerified": true,
                          "RefrenceUrl": model3.url,
                        },
                        callback: (model4) async {
                          if (model4 != null && mounted) {
                            if (model4.success) {
                              await PrefMgr.shared.setNationalIDCardStatus(
                                  TaskStatusCfg().getSatusCode(
                                      TaskStatusCfg.STATUS_UNVERIFIED));
                              showToast(
                                context: context,
                                msg: "NID Uploaded Successfully",
                              );
                              setState(() {});
                            }
                          }
                        });
                  }
                }
              });
        }
      },
    );
  }
}
