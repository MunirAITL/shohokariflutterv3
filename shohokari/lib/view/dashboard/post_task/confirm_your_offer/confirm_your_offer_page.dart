import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingsAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/more/resolutions/resolution_page.dart';
import 'package:aitl/view/dashboard/mytasks/offers/confirm_offer_page.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/ConfirmOfferController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'confirm_your_offer_base.dart';

class ConfirmYourOfferPage extends StatefulWidget {
  final TaskBiddingModel taskBiddingModel;
  const ConfirmYourOfferPage({Key key, this.taskBiddingModel})
      : super(key: key);

  @override
  State createState() => _ConfirmYourOfferPageState();
}

class _ConfirmYourOfferPageState
    extends BaseConfirmYourOfferStatefull<ConfirmYourOfferPage> {
  final confirmOfferController = Get.put(ConfirmOferController());
  bool isAlreadySubmitted = true;

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onPaused() {}

  @override
  void onResumed() {}

  @override
  void initState() {
    super.initState();
    try {
      appInit();
    } catch (e) {}
  }

  @override
  void dispose() {
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {}

  verifyPending(bool isBtnClicked) async {
    if (isBtnClicked) {
      wsUserProfile(
        userId: userData.userModel.id,
        callback: (model) {
          bool isYes = false;
          confirmDialog(
            context: context,
            title: "pending_verification".tr,
            msg: "no_yet_verified_user_message_message".tr,
            callbackYes: () async {
              try {
                final userModel = model.responseData.user;
                /*if (userModel.communityId == "2") {
                  if (widget.taskBiddingModel != null) {
                    Get.to(() =>
                        ConfirmOfferPage(taskBidding: widget.taskBiddingModel));
                  } else {
                    await APIViewModel().req<TaskBiddingsAPIModel>(
                        context: context,
                        url: APIMyTasksCfg.GET_TASKBIDDING_URL
                            .replaceAll("/#taskBiddingId#", ""),
                        reqType: ReqType.Get,
                        param: {
                          "Count": 1,
                          "IsAll": true,
                          "Page": 0,
                          "TaskId": 0,
                          "userId": userData.userModel.id,
                        },
                        callback: (model2) {
                          if (model2 != null) {
                            if (model2.success &&
                                model2.responseData.taskBiddings.length > 0) {
                              Get.to(() => ConfirmOfferPage(
                                  taskBidding:
                                      model2.responseData.taskBiddings[0]));
                            } else {
                              Get.back();
                            }
                          } else {
                            Get.back();
                          }
                        });
                  }
                } else {*/
                Get.to(() => ResolutionScreen());
                //}
              } catch (e) {
                Get.to(() => ResolutionScreen());
              }
            },
          );
        },
      );
    } else {
      Get.to(() => ResolutionScreen());
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          leading: IconButton(
              onPressed: () {
                Get.back();
              },
              icon: Icon(Icons.close, color: Colors.white)),
          title: UIHelper().drawAppbarTitle(title: 'confirm_your_offer'.tr),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "continue".tr,
            callback: () async {
              verifyPending(true);
            }),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    isAlreadySubmitted = true;
    return Container(
      child: ListView.builder(
          itemCount: confirmOfferController.listOffers.length,
          itemBuilder: (context, i) {
            final map = confirmOfferController.listOffers[i].obs;
            //bool isStatusIcon = false;
            if (i == 0) {
              //  profile pic
              if (userData.userModel.profileImageUrl.isEmpty ||
                  userData.userModel.profileImageUrl.contains(
                      "https://shohokari.com/api/content/media/default_avatar.png")) {
                map['status'] = false;
                isAlreadySubmitted = false;
              } else {
                map['status'] = true;
              }
            } else if (i == 1) {
              //  payment info / bank account
              PrefMgr.shared.getBankAccountStatus().then((status) {
                if (status ==
                    TaskStatusCfg()
                        .getSatusCode(TaskStatusCfg.STATUS_UNVERIFIED)) {
                  map['status'] = true;
                } else {
                  map['status'] = false;
                  isAlreadySubmitted = false;
                }
              });
            } else if (i == 2) {
              //  dob
              if (userData.userModel.dateofBirth.isEmpty) {
                map['status'] = false;
                isAlreadySubmitted = false;
              } else {
                map['status'] = true;
              }
            } else if (i == 3) {
              //  NID Card
              PrefMgr.shared.getNationalIDCardStatus().then((status) {
                if (status ==
                    TaskStatusCfg()
                        .getSatusCode(TaskStatusCfg.STATUS_UNVERIFIED)) {
                  map['status'] = true;
                } else {
                  map['status'] = false;
                  isAlreadySubmitted = false;
                }
              });
            }
            return Obx(() => Container(
                  child: ListTile(
                    onTap: () {
                      switch (i) {
                        case 0:
                          uploadProfilePicClicked(this.runtimeType);
                          break;
                        case 1:
                          paymentInfoClicked(this.runtimeType);
                          break;
                        case 2:
                          dobClicked(this.runtimeType);
                          break;
                        case 3:
                          uploadNIDClicked(this.runtimeType);
                          break;
                        default:
                      }
                    },
                    leading: Icon(map["icon"], color: MyTheme.hotdipPink),
                    title: Txt(
                        txt: map['title'],
                        txtColor: MyTheme.hotdipPink,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    trailing: map['status'] ? map['status_icon'] : SizedBox(),
                  ),
                ));
          }),
    );
  }
}
