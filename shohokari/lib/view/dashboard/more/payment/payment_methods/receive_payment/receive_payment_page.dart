import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/view/dashboard/more/payment/bank_account/add_bank_acc_page.dart';
import 'package:aitl/view/dashboard/more/payment/bkash/add_bkash_page.dart';
import 'package:aitl/view/dashboard/more/payment/payment_methods/receive_payment/add_billing_address_page.dart';
import 'package:aitl/view/dashboard/more/payment/rocket_account/add_rocket_page.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class ReceivePaymentPage extends StatefulWidget {
  final bool isHideAppbar;
  const ReceivePaymentPage({Key key, this.isHideAppbar = true})
      : super(key: key);

  @override
  State createState() => _ReceivePaymentPageState();
}

class _ReceivePaymentPageState extends State<ReceivePaymentPage> with UIHelper {
  List<String> listItem = [];

  bool isBankAccountDone = false;
  bool isBKashDone = false;
  bool isRocketDone = false;

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  appInit() async {
    await PrefMgr.shared.getBankAccountStatus().then((value) {
      if (value != null) {
        isBankAccountDone = true;
      }
    });
    await PrefMgr.shared.getBikashAccount().then((value) {
      if (value != null) {
        isBKashDone = true;
      }
    });
    await PrefMgr.shared.getRocketAccount().then((value) {
      if (value != null) {
        isRocketDone = true;
      }
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: (widget.isHideAppbar)
            ? null
            : AppBar(
                elevation: MyTheme.appbarElevation,
                backgroundColor: MyTheme.bgColor,
                iconTheme: MyTheme.themeData.iconTheme,
                title: UIHelper().drawAppbarTitle(
                    title: 'review_additional_funds_payment_methods'.tr),
                centerTitle: false,
              ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    listItem = [
      'add_billing_address'.tr.toUpperCase(),
      'add_bank_account_screen_title_add'.tr.toUpperCase(),
      'mobile_payment_screen_title_bikash'.tr.toUpperCase(),
      'mobile_payment_screen_title_rocket'.tr.toUpperCase(),
    ];
    return Container(
      color: Colors.white,
      child: ListView.builder(
        itemCount: listItem.length,
        itemBuilder: (BuildContext context, int index) {
          if (index == 0 && !widget.isHideAppbar) {
            return SizedBox();
          }
          final title = listItem[index];
          Widget wid = SizedBox();
          switch (index) {
            case 0:
              wid = Image.asset(
                "assets/images/icons/map_pin_icon.png",
                width: 20,
                height: 20,
                color: MyTheme.blueColor,
              );
              break;
            case 1:
              wid = SvgPicture.asset(
                'assets/images/svg/ic_card.svg',
                width: 20,
                height: 20,
                color: MyTheme.blueColor,
              );
              break;
            case 2:
              wid = Icon(
                Icons.mobile_friendly,
                size: 20,
                color: MyTheme.blueColor,
              );
              break;
            case 3:
              wid = Icon(
                Icons.mobile_friendly,
                size: 20,
                color: MyTheme.blueColor,
              );
              break;
            default:
          }
          return Padding(
            padding: const EdgeInsets.only(left: 10, right: 10),
            child: Container(
              color: Colors.transparent,
              child: ListTile(
                onTap: () {
                  switch (index) {
                    case 0:
                      Get.to(() => AddBillingAddrPage());
                      break;
                    case 1:
                      Get.to(() => AddBankAccountPage());
                      break;
                    case 2:
                      Get.to(() => AddBKashPage());
                      break;
                    case 3:
                      Get.to(() => AddRocketPage());
                      break;
                    default:
                  }
                },
                leading: wid,
                minLeadingWidth: 0,
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(height: 12),
                    Txt(
                        txt: title,
                        txtColor: MyTheme.hotdipPink,
                        txtSize: MyTheme.txtSize - .2,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(height: 20),
                    drawLine(),
                  ],
                ),
                trailing: (index > 0 &&
                        (isBankAccountDone || isBKashDone || isRocketDone))
                    ? SvgPicture.asset('assets/images/svg/ico_ok_sign.svg',
                        fit: BoxFit.cover, color: MyTheme.airGreenColor)
                    : SizedBox(),
              ),
            ),
          );
        },
      ),
    );
  }
}
