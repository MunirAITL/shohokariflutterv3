import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/server/APIPaymentCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/country_picker/ola_like_country_picker.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/BillingAddrAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/payment/methods/BillingAddressModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/input/InputBox.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'receive_payment_base.dart';

class AddBillingAddrPage extends StatefulWidget {
  const AddBillingAddrPage({Key key}) : super(key: key);
  @override
  State createState() => _AddBillingAddrState();
}

class _AddBillingAddrState
    extends BaseReceivePaymentStatefull<AddBillingAddrPage>
    with APIStateListener {
  final addressLine1 = TextEditingController();
  final addressLine2 = TextEditingController();
  final area = TextEditingController();
  final city = TextEditingController();
  final postCode = TextEditingController();
  //
  final focusAddr1 = FocusNode();
  final focusAddr2 = FocusNode();
  final focusArea = FocusNode();
  final focusCity = FocusNode();
  final focusPostCode = FocusNode();

  var countryName = '';

  BillingAddressModel billingAddressModel;

  CountryPicker c;
  Country country = Country.fromJson(countryCodes[94]);

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.payment_methods_receive_post_billing_addr &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            showToast(context: context, msg: 'billing_addr_added'.tr);
          }
        }
      }
      if (apiState.type == APIType.payment_methods_receive_put_billing_addr &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            showToast(
              context: context,
              msg: 'billing_addr_updated'.tr,
            );
          }
        }
      }
      if (apiState.type == APIType.payment_methods_receive_get_billing_addr &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            billingAddressModel =
                (model as BillingAddrAPIModel).responseData.billingAddress;
            addressLine1.text = billingAddressModel.address1.trim();
            addressLine2.text = billingAddressModel.address2.trim();
            city.text = billingAddressModel.city.trim();
            countryName = billingAddressModel.country.trim();
            postCode.text = billingAddressModel.zip.trim();
            area.text = billingAddressModel.state.trim();
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  wsAddBillingAddress() async {
    try {
      /*if (addressLine1.text.isEmpty) {
        showAlert(context:context,msg: 'Please enter a valid address line 1', isToast: true);
      } else if (area.text.isEmpty) {
        showAlert(context:context,msg: 'Please enter a valid area', isToast: true);
      } else if (city.text.isEmpty) {
        showAlert(context:context,msg: 'Please enter a valid city', isToast: true);
      } else if (postCode.text.isEmpty) {
        showAlert(context:context,msg: 'Please enter a valid post code', isToast: true);
      } else if (countryStr == '') {
        showAlert(context:context,msg: 'Please choose your country', isToast: true);
      } else {*/
      if (billingAddressModel != null) {
        APIViewModel().req<BillingAddrAPIModel>(
          context: context,
          apiState: APIState(APIType.payment_methods_receive_put_billing_addr,
              this.runtimeType, null),
          url: APIPaymentCfg.BILLING_ADDR_PUT_URL,
          param: {
            "Address1": addressLine1.text.trim(),
            "Address2": addressLine2.text.trim(),
            "City": city.text.trim(),
            "Country": countryName,
            "CreationDate": DateTime.now().toString(),
            "Id": billingAddressModel.id,
            "IsDefault": billingAddressModel.isDefault,
            "IsVerified": billingAddressModel.isVerified,
            "State": area.text.trim(),
            "Status": billingAddressModel.status,
            "UpdatedDate": DateTime.now().toString(),
            "UserId": userData.userModel.id,
            "Zip": postCode.text.trim(),
          },
          reqType: ReqType.Put,
        );
      } else {
        APIViewModel().req<BillingAddrAPIModel>(
          context: context,
          apiState: APIState(APIType.payment_methods_receive_post_billing_addr,
              this.runtimeType, null),
          url: APIPaymentCfg.BILLING_ADDR_POST_URL,
          param: {
            "Address1": addressLine1.text.trim(),
            "Address2": addressLine2.text.trim(),
            "City": city.text.trim(),
            "Country": countryName,
            "CreationDate": DateTime.now().toString(),
            "IsDefault": true,
            "IsVerified": false,
            "State": area.text.trim(),
            "Status": TaskStatusCfg.TASK_STATUS_ACTIVE,
            "UpdatedDate": DateTime.now().toString(),
            "UserId": userData.userModel.id,
            "Zip": postCode.text.trim(),
          },
          reqType: ReqType.Post,
        );
      }
      //}
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    addressLine1.dispose();
    addressLine2.dispose();
    area.dispose();
    city.dispose();
    postCode.dispose();
    countryName = null;
    billingAddressModel = null;
    try {
      c = null;
      country = null;
    } catch (e) {}
    try {
      myTaskController.dispose();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      c = CountryPicker(onCountrySelected: (Country country) {
        myLog(country);
        setState(() {
          this.country = country;
          countryName = country.name;
        });
      });
    } catch (e) {}

    try {
      APIViewModel().req<BillingAddrAPIModel>(
        context: context,
        apiState: APIState(APIType.payment_methods_receive_get_billing_addr,
            this.runtimeType, null),
        url: APIPaymentCfg.BILLING_ADDR_GET_URL,
        param: {"UserId": userData.userModel.id},
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(title: 'add_billing_address'.tr),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: "billing_address_button_change".tr,
            callback: () async {
              wsAddBillingAddress();
            }),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              InputBox(
                context: context,
                ctrl: addressLine1,
                lableTxt: "billing_address_au_address_line_1_hint".tr,
                labelColor: MyTheme.gray4Color,
                align: TextAlign.start,
                kbType: TextInputType.streetAddress,
                len: 255,
              ),
              SizedBox(height: 10),
              InputBox(
                ctrl: addressLine2,
                lableTxt: "billing_address_au_address_line_2_hint".tr,
                labelColor: MyTheme.gray4Color,
                align: TextAlign.start,
                kbType: TextInputType.streetAddress,
                len: 255,
              ),
              SizedBox(height: 10),
              InputBox(
                ctrl: area,
                lableTxt: "area".tr,
                labelColor: MyTheme.gray4Color,
                align: TextAlign.start,
                kbType: TextInputType.streetAddress,
                len: 50,
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: InputBox(
                      ctrl: city,
                      lableTxt: "city".tr,
                      labelColor: MyTheme.gray4Color,
                      align: TextAlign.start,
                      kbType: TextInputType.streetAddress,
                      len: 50,
                    ),
                  ),
                  SizedBox(width: 20),
                  Flexible(
                    child: InputBox(
                      ctrl: postCode,
                      lableTxt: "postcode".tr,
                      labelColor: MyTheme.gray4Color,
                      align: TextAlign.start,
                      kbType: TextInputType.streetAddress,
                      len: 10,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              GestureDetector(
                onTap: () {
                  c.launch(context);
                },
                child: Container(
                  width: getW(context),
                  decoration: BoxDecoration(
                    border: Border(
                      bottom: BorderSide(
                        color: Colors.black,
                        width: .5,
                      ),
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Txt(
                        txt: "country".tr,
                        txtColor: MyTheme.gray4Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false,
                      ),
                      SizedBox(height: 10),
                      Txt(
                        txt: " " + countryName,
                        txtColor: Colors.black,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false,
                      ),
                      SizedBox(height: 10),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }
}
