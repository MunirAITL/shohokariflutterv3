import 'package:aitl/config/server/APINotiCfg.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryDataModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/TaskSummaryDataAPIModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/TaskSummaryUserDataModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'db_base.dart';

class DashboardMore extends StatefulWidget {
  const DashboardMore({Key key}) : super(key: key);
  @override
  _DashboardMoreState createState() => _DashboardMoreState();
}

class _DashboardMoreState extends BaseDashboardMoreStatefull<DashboardMore>
    with APIStateListener, StateListener {
  List<UserRatingSummaryDataModel> listUserRatingSummary = [];
  TaskSummaryUserDataModel taskSummaryUserData;
  List<NotiModel> listNoti = [];

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value) AudioMgr().play("more_dashboard");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.user_rating_summary &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listUserRatingSummary = (model as UserRatingSummaryAPIModel)
                .responseData
                .userRatingSummaryData;

            try {
              profileController.setAverageRateAsPoster(
                  listUserRatingSummary[0].posterAverageRating.obs);
              profileController.setAverageRateAsTasker(
                  listUserRatingSummary[0].taskerAverageRating.obs);
              profileController.setCompletionRateAsPoster(
                  listUserRatingSummary[0].posterCompletionRate.obs);
              profileController.setCompletionRateAsTasker(
                  listUserRatingSummary[0].taskerCompletionRate.obs);
              profileController.setCountAsPoster(
                  listUserRatingSummary[0].completedPosterTaskCount.obs);
              profileController.setCountAsTasker(
                  listUserRatingSummary[0].completedTaskerTaskCount.obs);
            } catch (e) {
              myLog(e.toString());
            }
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.task_rating_summary &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            taskSummaryUserData = (model as TaskSummaryDataAPIModel)
                .responseData
                .taskSummaryUserData;
            setState(() {});
          }
        }
      }

      if (apiState.type == APIType.get_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listNoti = (model as NotiAPIModel).responseData.notifications;
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }

    listUserRatingSummary = null;
    taskSummaryUserData = null;
    listNoti = null;

    try {
      myTaskController.dispose();
      profileController.dispose();
    } catch (e) {}

    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
      StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    } catch (e) {}

    try {
      await APIViewModel().req<UserRatingSummaryAPIModel>(
        context: context,
        apiState: APIState(APIType.user_rating_summary, this.runtimeType, null),
        url: APIProfileCFg.GET_USER_RATING_SUMMARY_URL,
        reqType: ReqType.Get,
        param: {"UserId": userData.userModel.id},
      );
      await APIViewModel().req<TaskSummaryDataAPIModel>(
        context: context,
        apiState: APIState(APIType.task_rating_summary, this.runtimeType, null),
        url: APIPostTaskCfg.GET_TASK_SUMMARY_USERDATA_URL,
        param: {"UserId": userData.userModel.id},
        reqType: ReqType.Get,
      );
      await APIViewModel().req<NotiAPIModel>(
          context: context,
          apiState: APIState(APIType.get_noti, this.runtimeType, null),
          url: APINotiCfg.NOTI_GET_URL
              .replaceAll("#UserId#", userData.userModel.id.toString()),
          //param: {"UserId": userData.userModel.id},
          reqType: ReqType.Get);
    } catch (e) {}
  }

  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: MyTheme.gray1Color,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          iconTheme: MyTheme.themeData.iconTheme,
          backgroundColor: MyTheme.bgColor,
          title: UIHelper().drawAppbarTitle(title: 'dashboard'.tr),
          centerTitle: false,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return Container(
      color: MyTheme.gray1Color,
      child: SingleChildScrollView(
        child: Column(
          children: [
            drawUserSwitchView(),
            SizedBox(height: getHP(context, 20)),
            drawStats(taskSummaryUserData, listUserRatingSummary),
            drawNotiView(listNoti, false),
          ],
        ),
      ),
    );
  }
}
