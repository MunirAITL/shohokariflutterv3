import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/auth/UserModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/EntityPropertyAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/PublicProfileAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserBadgeAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserBadgesModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortFolioAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserPortfulioModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingSummaryDataModel.dart';
import 'package:aitl/data/model/dashboard/more/profile/UserRatingsModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/AudioController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'profile_base.dart';

class ProfilePage extends StatefulWidget {
  @override
  State createState() => _ProfilePageState();
}

class _ProfilePageState extends BaseProfileStatefull<ProfilePage>
    with APIStateListener, StateListener {
  List<UserRatingSummaryDataModel> listUserRatingSummary = [];
  List<UserRatingsModel> listUserRating = [];
  List<UserBadgesModel> listUserBadge = [];
  UserPortfulioModel userPortfulioModel;
  AboutModel aboutModel;
  UserModel userModel;
  MediaUploadFilesModel uploadFileModelCover;
  MediaUploadFilesModel uploadFileModelProfile;

  //  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value) AudioMgr().play("more_profile");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.entity_profile_cover_image &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsGetPublicProfile(userData.userModel.id);
          }
        }
      }
      if (apiState.type == APIType.entity_profile_image &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsGetPublicProfile(userData.userModel.id);
          }
        }
      }
      if (apiState.type == APIType.media_profile_cover_image &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            uploadFileModelCover =
                (model as MediaUploadFilesAPIModel).responseData.images[0];
            await APIViewModel().req<EntityPropertyAPIModel>(
              context: context,
              apiState: APIState(
                  APIType.entity_profile_cover_image, this.runtimeType, null),
              url: APIProfileCFg.ENTITY_PROPERTY_POST_URL,
              reqType: ReqType.Post,
              param: {
                "EntityId": userModel.id,
                "EntityName": "User",
                "PropertyName": "DefaultCoverId",
                "Value": uploadFileModelCover.id,
              },
            );
          }
        }
      }
      if (apiState.type == APIType.media_profile_image &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            uploadFileModelProfile =
                (model as MediaUploadFilesAPIModel).responseData.images[0];

            await APIViewModel().req<EntityPropertyAPIModel>(
              context: context,
              apiState: APIState(
                  APIType.entity_profile_image, this.runtimeType, null),
              url: APIProfileCFg.ENTITY_PROPERTY_POST_URL,
              reqType: ReqType.Post,
              param: {
                "EntityId": userModel.id,
                "EntityName": "User",
                "PropertyName": "DefaultPictureId",
                "Value": uploadFileModelProfile.id,
              },
            );
          }
        }
      }

      if (apiState.type == APIType.public_profile &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            userModel = (model as PublicProfileAPIModel).responseData.user;
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.user_rating_summary &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listUserRatingSummary = (model as UserRatingSummaryAPIModel)
                .responseData
                .userRatingSummaryData;

            try {
              profileController.setAverageRateAsPoster(
                  listUserRatingSummary[0].posterAverageRating.obs);
              profileController.setAverageRateAsTasker(
                  listUserRatingSummary[0].taskerAverageRating.obs);
              profileController.setCompletionRateAsPoster(
                  listUserRatingSummary[0].posterCompletionRate.obs);
              profileController.setCompletionRateAsTasker(
                  listUserRatingSummary[0].taskerCompletionRate.obs);
              profileController.setCountAsPoster(
                  listUserRatingSummary[0].completedPosterTaskCount.obs);
              profileController.setCountAsTasker(
                  listUserRatingSummary[0].completedTaskerTaskCount.obs);
            } catch (e) {
              myLog(e.toString());
            }
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.user_rating &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listUserRating =
                (model as UserRatingAPIModel).responseData.userRatings;
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.user_badge &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            listUserBadge =
                (model as UserBadgeAPIModel).responseData.userBadges;
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.portfolio &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            userPortfulioModel =
                (model as UserPortFolioAPIModel).responseData.userPortfulio;
            setState(() {});
          }
        }
      }
      if (apiState.type == APIType.about_skills &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            aboutModel = (model as AboutAPIModel).responseData.about;
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  wsGetPublicProfile(userId) async {
    try {
      await APIViewModel().req<PublicProfileAPIModel>(
        context: context,
        apiState: APIState(APIType.public_profile, this.runtimeType, null),
        url: APIProfileCFg.PUBLIC_USER_GET_URL
            .replaceAll("#userId#", userId.toString()),
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  refreshData() async {
    try {
      userModel = userData.userModel;
      if (myTaskController.getTaskModel().id != null) {
        if (myTaskController.getTaskModel().userId == userData.userModel.id) {
          isPublicUser = false;
          setState(() {});
        } else {
          userModel = null;
          wsGetPublicProfile(myTaskController.getTaskModel().userId);
        }
      } else {
        isPublicUser = false;
        wsGetPublicProfile(userModel.id);
      }
    } catch (e) {}
    try {
      await APIViewModel().req<UserRatingSummaryAPIModel>(
        context: context,
        apiState: APIState(APIType.user_rating_summary, this.runtimeType, null),
        url: APIProfileCFg.GET_USER_RATING_SUMMARY_URL,
        reqType: ReqType.Get,
        param: {
          "UserId": myTaskController.getTaskModel().userId ?? userModel.id
        },
      );
      await APIViewModel().req<UserRatingAPIModel>(
        context: context,
        apiState: APIState(APIType.user_rating, this.runtimeType, null),
        url: APIProfileCFg.USER_RATING_GET_URL,
        param: {
          "UserId": myTaskController.getTaskModel().userId ?? userModel.id
        },
        reqType: ReqType.Get,
      );
      print(myTaskController.getTaskModel().userId ?? userModel.id);
      await APIViewModel().req<UserBadgeAPIModel>(
        context: context,
        apiState: APIState(APIType.user_badge, this.runtimeType, null),
        url: APIProfileCFg.USER_BADGE_URL,
        param: {
          "UserId": myTaskController.getTaskModel().userId ?? userModel.id
        },
        reqType: ReqType.Get,
      );
      await APIViewModel().req<UserPortFolioAPIModel>(
        context: context,
        apiState: APIState(APIType.portfolio, this.runtimeType, null),
        url: APIProfileCFg.USER_PORTFOLIO_URL,
        param: {
          "UserId": myTaskController.getTaskModel().userId ?? userModel.id
        },
        reqType: ReqType.Get,
      );
      await APIViewModel().req<AboutAPIModel>(
        context: context,
        apiState: APIState(APIType.about_skills, this.runtimeType, null),
        url: APIProfileCFg.ABOUT_GET_URL,
        param: {
          "UserId": myTaskController.getTaskModel().userId ?? userModel.id
        },
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }

    userModel = null;
    listUserRatingSummary = null;
    listUserRating = null;
    listUserBadge = null;
    userPortfulioModel = null;
    aboutModel = null;
    uploadFileModelCover = null;
    uploadFileModelProfile = null;

    try {
      myTaskController.dispose();
      profileController.dispose();
    } catch (e) {}
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
      StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    } catch (e) {}

    refreshData();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        //backgroundColor: MyTheme.bgColor,
        //resizeToAvoidBottomPadding: true,
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: (userModel != null)
                ? drawHeader(userModel, this.runtimeType)
                : Container(
                    color: Colors.white,
                    width: getW(context),
                    height: getH(context),
                    child: Center(child: CircularProgressIndicator()),
                  )),
      ),
    );
  }

  drawLayout() {
    //  @UserProfileActivity in kotlin
    return Container(
      //color: MyTheme.bgColor,
      color: Colors.white,
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          SizedBox(height: 20),
          drawUserSwitchView(),
          SizedBox(height: 20),
          drawUserRatingAndSummaryView(
              userModel, listUserRatingSummary, listUserRating),
          drawPortFolioView(userPortfulioModel),
          drawBadgesView(listUserBadge),
          drawMoreButtonView(),
          drawAboutSkillsView(aboutModel),
          SizedBox(height: 50),
        ],
      ),
    );
  }
}
