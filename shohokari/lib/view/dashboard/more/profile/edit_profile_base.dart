import 'dart:io';

import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/classes/DateFun.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/profile/AboutModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/dashboard/more/profile/add_skills_page.dart';
import 'package:aitl/view/widgets/datepicker/DatePickerView.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/google/GPlacesView.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/input/InputBox.dart';
import 'package:aitl/view/widgets/input/InputBoxHT.dart';
import 'package:aitl/view/widgets/input/InputMobFlagBox.dart';
import 'package:aitl/view/widgets/picker/CamPicker.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_webservice/geocoding.dart';
import 'package:intl/intl.dart';

abstract class BaseEditProfileStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  //UserPortfulioModel userPortfulioModel;

  AboutModel aboutModel;

  //  gen info
  final fname = TextEditingController();
  final lname = TextEditingController();
  final headline = TextEditingController();
  final bio = TextEditingController();
  final email = TextEditingController();
  //
  final focusFname = FocusNode();
  final focusLname = FocusNode();
  final focusHeadline = FocusNode();
  final focusBio = FocusNode();
  final focusEmail = FocusNode();
  final focusMobile = FocusNode();

  String address = "search".tr;
  Location cord;
  String dob = "";

  final mobile = TextEditingController();
  var countryDialCode = AppDefine.COUNTRY_DIALCODE;

  //  portfolio upload file list
  List<String> listUrl = [];
  bool isPortfolioUploaded = false;

  wsGetAboutSkills();

  _drawHeading(
      {String title, Color txtColor, Color bgColor, double txtSizePlus = 0}) {
    if (txtColor == null) {
      txtColor = MyTheme.gray5Color;
    }
    if (bgColor == null) {
      bgColor = MyTheme.gray2Color;
    }
    return Container(
      color: bgColor,
      width: getW(context),
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Txt(
            txt: title,
            txtColor: txtColor,
            txtSize: MyTheme.txtSize + txtSizePlus,
            txtAlign: TextAlign.start,
            isBold: true),
      ),
    );
  }

  drawGenInfo() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _drawHeading(title: "general_information_label".tr),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: [
                InputBox(
                  context: context,
                  ctrl: fname,
                  lableTxt: "create_profile_first_name_hint".tr,
                  kbType: TextInputType.name,
                  inputAction: TextInputAction.next,
                  focusNode: focusFname,
                  focusNodeNext: focusLname,
                  len: 20,
                  ecap: eCap.Word,
                ),
                SizedBox(height: 10),
                InputBox(
                  context: context,
                  ctrl: lname,
                  lableTxt: "create_profile_last_name_hint".tr,
                  kbType: TextInputType.name,
                  inputAction: TextInputAction.next,
                  focusNode: focusLname,
                  focusNodeNext: focusHeadline,
                  len: 20,
                  ecap: eCap.Word,
                ),
                //SizedBox(height: 10),
                GPlacesView(
                  title: "browse_tasks_map_location_label".tr,
                  address: address,
                  //txtColor: MyTheme.gray4Color,
                  bgColor: Colors.white,
                  isTxtBold: false,
                  titlePadding: 5,
                  callback: (String _address, Location _loc) {
                    //callback(address);
                    address = _address;
                    cord = _loc;
                    setState(() {});
                  },
                ),
                SizedBox(height: 10),
                InputBox(
                  context: context,
                  ctrl: headline,
                  lableTxt: "headline".tr,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  focusNode: focusHeadline,
                  focusNodeNext: focusBio,
                  len: 255,
                  ecap: eCap.Sentence,
                ),
                SizedBox(height: 10),
                InputBox(
                  context: context,
                  ctrl: bio,
                  lableTxt: "edit_profile_about_me_label".tr,
                  kbType: TextInputType.text,
                  inputAction: TextInputAction.next,
                  focusNode: focusBio,
                  focusNodeNext: focusEmail,
                  len: 255,
                  ecap: eCap.Sentence,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  drawPvtInfo() {
    final DateTime dateNow = DateTime.now();
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _drawHeading(title: "private_information_label".tr),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: [
                InputBox(
                  ctrl: email,
                  lableTxt: "email_txt".tr,
                  kbType: TextInputType.emailAddress,
                  inputAction: TextInputAction.next,
                  focusNode: focusEmail,
                  focusNodeNext: focusMobile,
                  len: 50,
                  suffixIcon: (!userData.userModel.isEmailVerified)
                      ? GestureDetector(
                          onTap: () {
                            FocusScope.of(context).requestFocus(FocusNode());
                            showAlert(
                              context: context,
                              msg: 'email_not_verified'.tr,
                            );
                          },
                          child: Container(
                            width: 50,
                            height: 40,
                            color: Colors.transparent,
                            child: Icon(
                              Icons.info,
                              color: Colors.deepOrange,
                            ),
                          ),
                        )
                      : null,
                ),
                SizedBox(height: 10),
                DatePickerView(
                  cap: "dob".tr,
                  dt: dob,
                  txtColor: Colors.black,
                  borderWidth: 1,
                  initialDate:
                      DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
                  firstDate:
                      DateTime(dateNow.year - 100, dateNow.month, dateNow.day),
                  lastDate:
                      DateTime(dateNow.year - 18, dateNow.month, dateNow.day),
                  callback: (value) {
                    if (mounted) {
                      setState(() {
                        try {
                          dob = DateFormat(DateFun.getDOBFormat())
                              .format(value)
                              .toString();
                        } catch (e) {
                          myLog(e.toString());
                        }
                      });
                    }
                  },
                ),
                SizedBox(height: 20),
              ],
            ),
          ),
        ],
      ),
    );
  }

  drawAdditionalInfo() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _drawHeading(title: "additional_information_label".tr),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                Txt(
                    txt: "current_mobile_number_label".tr,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize,
                    txtAlign: TextAlign.start,
                    isBold: false),
                InputMobFlagBox(
                  ctrl: mobile,
                  lableTxt: "mobile_number_label".tr,
                  inputAction: TextInputAction.done,
                  focusNode: focusMobile,
                  len: 15,
                  getCountryCode: (code) {
                    countryDialCode = code.toString();
                    PrefMgr.shared.setPrefStr("countryName", code.code);
                    PrefMgr.shared.setPrefStr("countryCode", code.toString());
                  },
                ),
                SizedBox(height: 10),
              ],
            ),
          ),
        ],
      ),
    );
  }

  drawPortFolio(cls) {
    final len = listUrl.length;
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _drawHeading(
              title: "portfolio".tr,
              txtColor: MyTheme.gray4Color,
              bgColor: Colors.white,
              txtSizePlus: .3),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Container(
              child: GridView.count(
                shrinkWrap: true,
                primary: false,
                crossAxisCount: 3,
                crossAxisSpacing: 10,
                mainAxisSpacing: 10,
                padding: EdgeInsets.all(10.0),
                children: List.generate(
                  len + ((len >= AppConfig.totalUploadLimit) ? 0 : 1),
                  (index) {
                    return Card(
                      child: (index < len)
                          ? Stack(
                              children: <Widget>[
                                Container(
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      image: MyNetworkImage.loadProfileImage(
                                          listUrl[index]),
                                      fit: BoxFit.cover,
                                    ),
                                    shape: BoxShape.rectangle,
                                  ),
                                ),
                                Positioned(
                                  right: -5,
                                  top: -5,
                                  child: Container(
                                    width: 30,
                                    height: 30,
                                    child: MaterialButton(
                                      onPressed: () {
                                        confirmDialog(
                                          context: context,
                                          title: "portfolio_item_delete".tr,
                                          msg: "portfolio_del_confirm".tr,
                                          callbackYes: () {
                                            listUrl.remove(listUrl[index]);
                                            isPortfolioUploaded = true;
                                            setState(() {});
                                          },
                                        );
                                      },
                                      color: MyTheme.gray1Color,
                                      child: Icon(Icons.close,
                                          color: Colors.black),
                                      padding: EdgeInsets.all(0),
                                      shape: CircleBorder(),
                                    ),
                                  ),
                                ),
                              ],
                            )
                          : GestureDetector(
                              onTap: () {
                                CamPicker().showCamDialog(
                                  context: context,
                                  isRear: false,
                                  callback: (File path) {
                                    if (path != null) {
                                      APIViewModel().upload(
                                        context: context,
                                        apiState: APIState(
                                            APIType.media_upload_file,
                                            cls,
                                            null),
                                        file: path,
                                      );
                                    }
                                  },
                                );
                              },
                              child: Container(
                                decoration: BoxDecoration(
                                  color: MyTheme.gray1Color,
                                ),
                                child: Icon(
                                  Icons.add,
                                  size: 100,
                                  color: MyTheme.gray3Color,
                                ),
                              ),
                            ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawAboutSkills() {
    if (aboutModel == null) return SizedBox();
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _drawHeading(
              title: "skills_label".tr,
              txtColor: MyTheme.gray4Color,
              bgColor: Colors.white,
              txtSizePlus: .3),
          _drawSkillRow("Transportation", aboutModel.goAround ?? ''),
          _drawSkillRow("Languages", aboutModel.languages ?? ''),
          _drawSkillRow("Education", aboutModel.qualifications ?? ''),
          _drawSkillRow("Work", aboutModel.experiences ?? ''),
          _drawSkillRow("Specialities", aboutModel.whatIamlookingfor ?? ''),
        ],
      ),
    );
  }

  _drawSkillRow(String title, String skills) {
    var title2 = "";
    var hint = "";
    switch (title) {
      case "Transportation":
        title2 = "transportation_label".tr;
        hint = "means_transport".tr;
        break;
      case "Languages":
        title2 = "languages_label".tr;
        hint = "languages_versed".tr;
        break;
      case "Education":
        title2 = "education_label".tr;
        hint = "education_input_hint".tr;
        break;
      case "Work":
        title2 = "work_label".tr;
        hint = "work_input_hint".tr;
        break;
      case "Specialities":
        title2 = "specialities_label".tr;
        hint = "specialities_input_hint".tr;
        break;
      default:
    }
    return GestureDetector(
      onTap: () {
        Get.to(() => AddSkillsPage(
              aboutModel: aboutModel,
              skills: skills,
              title2: title2,
              title: title,
              hint: hint,
            )).then((value) => {wsGetAboutSkills()});
      },
      child: Container(
        color: Colors.transparent,
        child: Padding(
          padding: const EdgeInsets.only(left: 10, bottom: 10),
          child: Column(
            children: [
              ListTile(
                title: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Txt(
                        txt: title2,
                        txtColor: MyTheme.gray5Color,
                        txtSize: MyTheme.txtSize,
                        txtAlign: TextAlign.start,
                        isBold: false),
                    SizedBox(width: 10),
                    Expanded(
                      child: Txt(
                          txt: skills.replaceAll("|", ", "),
                          txtColor: MyTheme.gray4Color,
                          txtSize: MyTheme.txtSize - .2,
                          txtAlign: TextAlign.end,
                          isOverflow: true,
                          isBold: false),
                    ),
                    SizedBox(width: 5),
                    Icon(
                      Icons.arrow_forward_ios,
                      color: MyTheme.gray3Color,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
