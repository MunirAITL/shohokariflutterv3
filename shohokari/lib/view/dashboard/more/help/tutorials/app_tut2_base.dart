import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BaseTut2Statefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  double currentPage = 0;
  PageController controller = PageController(
    initialPage: 0,
  );
  int pageCount = 4;

  onGetStarted();

  drawPage1() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: 'hi'.tr +
                        ' ' +
                        userData.userModel.firstName +
                        ", " +
                        "lets_get_earning_title".tr,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + 1.5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              SizedBox(height: 30),
              Container(
                width: getWP(context, 70),
                height: getHP(context, 40),
                child: Image.asset(
                  "assets/images/tutorial/demo1.png",
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: "on_boarding_runner_one_message".tr,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    txtLineSpace: 1.3,
                    isBold: false),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawPage2() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: "on_boarding_tasker_two_title".tr,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + 1.5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              SizedBox(height: 30),
              Container(
                width: getWP(context, 70),
                height: getHP(context, 40),
                child: Image.asset(
                  "assets/images/tutorial/demo2.png",
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: "on_boarding_tasker_two_message".tr,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    txtLineSpace: 1.3,
                    isBold: false),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawPage3() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: "on_boarding_tasker_three_title".tr,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + 1.5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              SizedBox(height: 30),
              Container(
                width: getWP(context, 70),
                height: getHP(context, 40),
                child: Image.asset(
                  "assets/images/tutorial/demo3.png",
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: "on_boarding_tasker_three_message".tr,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    txtLineSpace: 1.3,
                    isBold: false),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawPage4() {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(20),
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: "on_boarding_tasker_four_title".tr,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + 1.5,
                    txtAlign: TextAlign.center,
                    isBold: true),
              ),
              SizedBox(height: 30),
              Container(
                width: getWP(context, 70),
                height: getHP(context, 40),
                child: Image.asset(
                  "assets/images/tutorial/demo4.png",
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(height: 50),
              Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Txt(
                    txt: "on_boarding_tasker_four_message".tr,
                    txtColor: MyTheme.gray5Color,
                    txtSize: MyTheme.txtSize + .3,
                    txtAlign: TextAlign.center,
                    txtLineSpace: 1.3,
                    isBold: false),
              ),
              SizedBox(height: 20),
            ],
          ),
        ),
      ),
    );
  }

  drawBottomBar() {
    return BottomAppBar(
      color: MyTheme.bgColor,
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            DotsIndicator(
              dotsCount: pageCount,
              position: currentPage,
              decorator: DotsDecorator(
                color: Colors.black87, // Inactive color
                activeColor: Colors.redAccent,
              ),
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                MMBtn(
                    txt: (currentPage == 0) ? "back".tr : "previous".tr,
                    height: getHP(context, MyTheme.btnHpa),
                    width: getWP(context, 42),
                    radius: 20,
                    callback: () {
                      if (currentPage == 0)
                        Get.back();
                      else {
                        controller.previousPage(
                            duration: Duration(milliseconds: 100),
                            curve: Curves.decelerate);
                      }
                    }),
                MMBtn(
                    txt: (currentPage == pageCount - 1)
                        ? "on_boarding_poster_four_continue_button_label".tr
                        : "next_btn".tr,
                    height: getHP(context, MyTheme.btnHpa),
                    width: getWP(context, 42),
                    radius: 20,
                    callback: () {
                      if ((currentPage == pageCount - 1)) {
                        onGetStarted();
                      } else {
                        controller.nextPage(
                            duration: Duration(milliseconds: 100),
                            curve: Curves.decelerate);
                      }
                    }),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
