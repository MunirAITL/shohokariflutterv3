import 'package:aitl/view/dashboard/dashboard_base.dart';
import 'package:aitl/view_model/rx/MyTaskController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BaseResStatefull<T extends StatefulWidget>
    extends BaseDashboard<T> {
  final myTaskController = Get.put(MyTaskController());
}
