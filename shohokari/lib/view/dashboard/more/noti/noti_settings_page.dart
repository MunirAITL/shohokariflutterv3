import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APINotiCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/app_data/CommonData.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/more/noti/NotiTestAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/UserNotificationSettingAPIModel.dart';
import 'package:aitl/data/model/dashboard/more/noti/UserNotificationSettingModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/dashboard/more/noti/noti_settings_update_page.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'noti_settings_base.dart';

class NotiSettingsPage extends StatefulWidget {
  const NotiSettingsPage({Key key}) : super(key: key);
  @override
  State createState() => _NotiSettingsPageState();
}

class _NotiSettingsPageState extends NotiSettingsStatefull<NotiSettingsPage>
    with APIStateListener, StateListener {
  UserNotificationSettingModel userNotificationSettingModel;

  //  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value)
          AudioMgr().play("setting_notification_settings");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.user_noti_settings_get &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            userNotificationSettingModel =
                (model as UserNotificationSettingAPIModel)
                    .responseData
                    .userNotificationSetting;
            setState(() {});
          }
        }
      }
    } catch (e) {}
  }

  wsGetNotiSettings() async {
    try {
      await APIViewModel().req<UserNotificationSettingAPIModel>(
        context: context,
        apiState:
            APIState(APIType.user_noti_settings_get, this.runtimeType, null),
        url: APINotiCfg.NOTI_SETTINGS_GET_URL,
        param: {"UserId": userData.userModel.id},
        reqType: ReqType.Get,
      );
    } catch (e) {}
  }

  wsTestIt() async {
    try {
      await APIViewModel().req<NotiTestAPIModel>(
          context: context,
          url: APINotiCfg.NOTI_SETTINGS_TEST_URL
              .replaceAll("#userId#", userData.userModel.id.toString()),
          reqType: ReqType.Get,
          callback: (model) {
            comData.isNotiTestPage = true;
            //print(model);
            /*try {
              final notification = model.responseData.notification;
              showAlert(context:context,
                  msg: notification.message + "\n\n" + notification.description,
                  isToast: true);
            } catch (e) {}*/
          });
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    userNotificationSettingModel = null;
    comData.isNotiTestPage = false;
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
      StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    } catch (e) {}

    wsGetNotiSettings();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper()
              .drawAppbarTitle(title: 'notifications_settings_screen_title'.tr),
          centerTitle: false,
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  @override
  drawLayout() {
    return Container(
      child: SingleChildScrollView(
        primary: true,
        child: Column(
          children: [
            drawTest(),
            drawList(),
          ],
        ),
      ),
    );
  }

  drawTest() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Txt(
                  txt: "notifications_settings_test_header".tr,
                  txtColor: MyTheme.gray4Color,
                  txtSize: MyTheme.txtSize,
                  txtAlign: TextAlign.center,
                  isBold: false,
                ),
                TextButton(
                    onPressed: () {
                      wsTestIt();
                    },
                    child: Txt(
                      txt: "notifications_settings_test_send_button".tr,
                      txtColor: MyTheme.blueColor.withOpacity(.7),
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false,
                    )),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt: "notifications_settings_test_description".tr,
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false,
            ),
          ),
          SizedBox(height: 10),
          drawLine(h: 1),
          SizedBox(height: 10),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Txt(
              txt: "notifications_settings_section".tr,
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false,
            ),
          ),
          SizedBox(height: 10),
          drawLine(h: 1),
        ],
      ),
    );
  }

  drawList() {
    if (userNotificationSettingModel == null) return SizedBox();
    final List<String> listItems = [
      "notifications_settings_label_transactional".tr,
      "notifications_settings_label_task_updates".tr,
      "notifications_settings_label_task_reminders".tr,
      AppDefine.APP_NAME + " " + "alerts".tr,
      "notifications_settings_label_task_recommendations".tr,
      "notifications_settings_label_helpful_information".tr,
      "notifications_settings_label_updates_newsletters".tr,
    ];
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemCount: listItems.length,
      itemBuilder: (context, i) {
        var item = '';
        var title = '';
        var heading = '';
        bool isEmail;
        bool isSms;
        bool isPush;
        switch (enumNotiSettings.values[i]) {
          case enumNotiSettings.Transactional: //  Transactional
            title = listItems[0];
            heading = "notifications_settings_transactional_details".tr;
            isEmail = userNotificationSettingModel.isTransactionalEmail;
            isSms = userNotificationSettingModel.isTransactionalSMS;
            isPush = userNotificationSettingModel.isTransactionalNotification;
            item = _makeNotiString(isEmail, isSms, isPush);
            break;
          case enumNotiSettings.TaskUpdates: //  Task updates
            title = listItems[1];
            heading = "notifications_settings_task_updates_details".tr;
            isEmail = userNotificationSettingModel.isTaskUpdateEmail;
            isSms = userNotificationSettingModel.isTaskUpdateSMS;
            isPush = userNotificationSettingModel.isTaskUpdateNotification;
            item = _makeNotiString(isEmail, isSms, isPush);
            break;
          case enumNotiSettings.TaskReminders: //  Task reminders
            title = listItems[2];
            heading = "notifications_settings_task_reminders_details".tr;
            isEmail = userNotificationSettingModel.isTaskReminderEmail;
            isSms = userNotificationSettingModel.isTaskReminderSMS;
            isPush = userNotificationSettingModel.isTaskReminderNotification;
            item = _makeNotiString(isEmail, isSms, isPush);
            break;
          case enumNotiSettings.AppAlerts: //  HeroTasker alerts
            title = listItems[3];
            heading = "notifications_settings_details_Shohokari_alerts".tr;
            isEmail = userNotificationSettingModel.isShohokariAlertEmail;
            isPush = userNotificationSettingModel.isShohokariAlertNotification;
            item = _makeNotiString(isEmail, false, isPush);
            break;
          case enumNotiSettings.TaskRecommendations: //  Task recommendations
            title = listItems[4];
            heading = "notifications_settings_details_task_recommendations".tr;
            isEmail = userNotificationSettingModel.isTaskRecomendationEmail;
            isPush =
                userNotificationSettingModel.isTaskRecomendationNotification;
            item = _makeNotiString(isEmail, false, isPush);
            break;
          case enumNotiSettings.HelpfulInfo: //  Helpful information
            title = listItems[5];
            heading = "";
            isEmail = userNotificationSettingModel.isHelpFullEmail;
            isPush = userNotificationSettingModel.isHelpFullNotification;
            item = _makeNotiString(isEmail, false, isPush);
            break;
          case enumNotiSettings.UpdateNewsLettters: //  Updates & newsletters
            title = listItems[6];
            heading = "";
            heading = "notifications_settings_updates_newsletters_details".tr;
            isPush =
                userNotificationSettingModel.isUpdateAndNewsLetterNotification;
            item = _makeNotiString(false, false, isPush);
            break;
          default:
        }
        return GestureDetector(
          onTap: () {
            Get.to(() => NotiSettingsUpdatePage(
                    index: i,
                    title: title,
                    heading: heading,
                    isEmail: isEmail,
                    isSms: isSms,
                    isPush: isPush,
                    userNotificationSettingModel: userNotificationSettingModel))
                .then((value) {
              if (value) wsGetNotiSettings();
            });
          },
          child: Container(
            color: Colors.transparent,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                      left: 20, right: 10, top: 20, bottom: 20),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Txt(
                          txt: listItems[i],
                          txtColor: MyTheme.gray5Color,
                          txtSize: MyTheme.txtSize,
                          txtAlign: TextAlign.start,
                          isBold: false,
                        ),
                        Expanded(
                          child: Txt(
                            txt: item,
                            txtColor: MyTheme.gray4Color,
                            txtSize: MyTheme.txtSize - .2,
                            txtAlign: TextAlign.end,
                            isBold: false,
                          ),
                        ),
                        SizedBox(width: 5),
                        Icon(
                          Icons.arrow_forward_ios,
                          color: MyTheme.gray3Color,
                          size: 20,
                        ),
                      ]),
                ),
                drawLine(),
              ],
            ),
          ),
        );
      },
    );
  }

  _makeNotiString(isEmail, isSms, isNoti) {
    String item = "";
    try {
      if (isEmail) item = "email_noti".tr + ", ";
      if (isSms) item += "sms_noti".tr + ", ";
      if (isNoti) item += "push_noti".tr + ", ";
      if (item.endsWith(", ")) {
        var pos = item.lastIndexOf(',');
        item = (pos != -1) ? item.substring(0, pos) : item;
      }
    } catch (e) {}
    return item;
  }
}
