import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIMyTasksCfg.dart';
import 'package:aitl/config/server/APIPaymentCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/mytasks/offers/UpdateOfferTaskBiddingAPIModel.dart';
import 'package:aitl/data/model/dashboard/payment_release/PostSSLMobileAppValidationAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/widgets/btn/BottomBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropDownListDialog.dart';
import 'package:aitl/view/widgets/dropdown/DropListModel.dart';
import 'package:aitl/view/widgets/images/MyNetworkImage.dart';
import 'package:aitl/view/widgets/switchview/ToggleSwitch.dart';
import 'package:aitl/view/widgets/txt/PriceBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_sslcommerz/model/SSLCCustomerInfoInitializer.dart';
import 'package:flutter_sslcommerz/model/SSLCSdkType.dart';
import 'package:flutter_sslcommerz/model/SSLCommerzInitialization.dart';
import 'package:flutter_sslcommerz/model/SSLCurrencyType.dart';
import 'package:flutter_sslcommerz/sslcommerz.dart';
import 'package:get/get.dart';

import 'fund_payment_base.dart';
//import 'package:flutter_sslcommerz/flutter_sslcommerz.dart';
//  https://medium.com/47billion/creating-a-bridge-in-flutter-between-dart-and-native-code-in-java-or-objectivec-5f80fd0cd713
// https://github.com/sslcommerz/SSLCommerz-Flutter

class FundPaymentPage extends StatefulWidget {
  final TaskBiddingModel taskBidding;
  final bool isReleasePayment;
  const FundPaymentPage(
      {Key key, this.taskBidding, this.isReleasePayment = false})
      : super(key: key);
  @override
  State createState() => _FundPaymentPageState();
}

class _FundPaymentPageState extends BasePaymentStatefull<FundPaymentPage>
    with Mixin, UIHelper, APIStateListener {
  int switchIndex = 0;
  bool isCash = false;
  double price = 0;

  //static const platform = const MethodChannel('flutter.native/payment');

  DropListModel ddTitle = DropListModel([
    OptionItem(id: 1, title: "Bkash Mobile Banking"),
    OptionItem(id: 2, title: "DBBL Mobile Banking"),
    OptionItem(id: 3, title: "BRAC VISA"),
    OptionItem(id: 4, title: "Dutch Bangla VISA"),
    OptionItem(id: 5, title: "City Bank Visa"),
    OptionItem(id: 6, title: "EBL Visa"),
    OptionItem(id: 7, title: "Southeast Bank Visa"),
    OptionItem(id: 8, title: "BRAC MASTER"),
    OptionItem(id: 9, title: "MASTER Dutch-Bangla"),
    OptionItem(id: 10, title: "City Master Card"),
    OptionItem(id: 11, title: "EBL Master Card"),
    OptionItem(id: 12, title: "Southeast Bank Master Card"),
    OptionItem(id: 13, title: "City Bank AMEX"),
    OptionItem(id: 14, title: "QCash"),
    OptionItem(id: 15, title: "DBBL Nexus"),
    OptionItem(id: 16, title: "Bank Asia IB"),
    OptionItem(id: 17, title: "AB Bank IB"),
    OptionItem(id: 18, title: "IBBL IB and Mobile Banking"),
    OptionItem(id: 19, title: "Mutual Trust Bank IB"),
    OptionItem(id: 20, title: "City Touch IB"),
  ]);

  OptionItem optTitle =
      OptionItem(id: null, title: "funded_payment_methods".tr);

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.stripe_payment_intent_post &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {}
        }
      } else if (apiState.type == APIType.task_bidding_put &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            Get.back();
          }
        }
      }
    } catch (e) {}
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  appInit() async {
    try {
      //StripeMgr().initStripe();
    } catch (e) {}
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}

    try {
      if (widget.isReleasePayment) {
        /*await APIViewModel().req<GetTaskPaymentByTaskIdTaskBiddingIdAPIModel>(
            context: context,
            url: APIPaymentCfg
                .TASK_PAYMENTS_GET_TASK_PAYMENT_TASKID_TASKBIDDINGID_URL,
            isLoading: true,
            reqType: ReqType.Post,
            param: {
              "TaskId": myTaskController.getTaskModel().id,
            },
            callback: (model) {
              if (model != null && mounted) {
                if (model.success) {}
              }
            });*/
      }
    } catch (e) {}
  }

  onCashPayment() {
    try {
      APIViewModel().req<UpdateOfferTaskBiddingAPIModel>(
          context: context,
          apiState: APIState(APIType.task_bidding_put, this.runtimeType, null),
          url: APIMyTasksCfg.PUT_TASKBIDDING_URL,
          //isLoading: false,
          reqType: ReqType.Put,
          param: {
            "CoverLetter": widget.taskBidding.description,
            "DeliveryDate":
                widget.taskBidding.deliveryDate ?? DateTime.now().toString(),
            "DeliveryTime":
                widget.taskBidding.deliveryTime ?? DateTime.now().toString(),
            "Description": widget.taskBidding.description ?? '',
            "DiscountAmount": widget.taskBidding.discountAmount ?? 0,
            "FixedbiddigAmount": widget.taskBidding.fixedbiddigAmount,
            "HourlyRate": myTaskController.getTaskModel().hourlyRate ?? 0.0,
            "NetTotalAmount":
                myTaskController.getTaskModel().netTotalAmount ?? 0.0,
            "PaymentStatus": TaskStatusCfg.STATUS_PAYMENT_METHOD_CASH,
            "ReferenceId": widget.taskBidding.referenceId ?? 0,
            "ReferenceType": widget.taskBidding.referenceType ?? 0,
            "ServiceFeeAmount": widget.taskBidding.serviceFeeAmount ?? 0.0,
            "ShohokariAmount": widget.taskBidding.shohokariAmount ?? 0.0,
            "Status": TaskStatusCfg.TASK_STATUS_ACCEPTED,
            "Id": widget.taskBidding.id ?? 0,
            "TaskId": myTaskController.getTaskModel().id,
            "TotalHour": myTaskController.getTaskModel().totalHours,
            "TotalHourPerWeek": widget.taskBidding.totalHourPerWeek ?? 0.0,
            "UserId": userData.userModel.id,
            "UserPromotionId": widget.taskBidding.userPromotionId ?? 0,
          });
    } catch (e) {}
  }

  onCardBankPayment() async {
    try {
      Sslcommerz sslcommerz = Sslcommerz(
          initializer: SSLCommerzInitialization(
              //   ipn_url: "www.ipnurl.com",
              multi_card_name: optTitle.title ?? 'BRAC VISA',
              currency: SSLCurrencyType.BDT,
              product_category: myTaskController.getTaskModel().title ??
                  widget.taskBidding.description,
              sdkType: (Server.isTest) ? SSLCSdkType.TESTBOX : SSLCSdkType.LIVE,
              store_id: (Server.isTest) ? "aitl5a969337b079b" : "shohokarilive",
              store_passwd: (Server.isTest)
                  ? "aitl5a969337b079b@ssl"
                  : "5AF2F531375BB64072",
              total_amount: price,
              tran_id: widget.taskBidding.id.toString()));
      sslcommerz.addCustomerInfoInitializer(
          customerInfoInitializer: SSLCCustomerInfoInitializer(
        customerState: "",
        customerName:
            userData.userModel.firstName + ' ' + userData.userModel.lastName,
        customerEmail: userData.userModel.email,
        customerAddress1: userData.userModel.address,
        customerCity: "",
        customerPostCode: "",
        customerCountry: AppDefine.COUNTRY_NAME,
        customerPhone: userData.userModel.mobileNumber,
      ));
      final result = await sslcommerz.payNow();
      print(result);
      if (result is PlatformException) {
        print("the response is: " +
            result.message.toString() +
            " code: " +
            result.code);
        showToast(context: context, msg: result.message.toString(), which: 0);
      } else {
        //SSLCTransactionInfoModel model = result;
        APIViewModel().req<PostSSLMobileAppValidationAPIModel>(
            context: context,
            url: APIPaymentCfg.TASKPAYMENT_POSTSSLVALIDATION_URL,
            //isLoading: false,
            reqType: ReqType.Post,
            param: result,
            callback: (model) {
              if (model != null && mounted) {
                if (model.success) {
                  Get.back();
                }
              }
            });
      }
    } catch (e) {
      showToast(context: context, msg: e.toString());
    }
    /*try {      
      final param = {
        'fName': userData.userModel.firstName,
        'lName': userData.userModel.lastName,
        'email': userData.userModel.email,
        'mobile': userData.userModel.mobileNumber,
        'netTotalAmount': price,
        'taskbiddingId': widget.taskBidding.id,
        'cardType': optTitle.title ?? 'BRAC VISA'
      };
      final String result =
          await platform.invokeMethod('paymentAPIActivity', param);
      if (result != null) {
        APIViewModel().req<PostSSLMobileAppValidationAPIModel>(
            context: context,
            url: APIPaymentCfg.TASKPAYMENT_POSTSSLVALIDATION_URL,
            //isLoading: false,
            reqType: ReqType.Post,
            param: json.decode(result),
            callback: (model) {
              if (model != null && mounted) {
                if (model.success) {
                  Get.back();
                }
              }
            });
      } else {
        showAlert(context:context,msg: "Failed to release payment", isToast: true, which: 0);
      }
    } on PlatformException catch (e) {
      showAlert(context:context,msg: "Failed to Invoke: '${e.message}'.", which: 0);
    }*/
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(
              title: (widget.isReleasePayment)
                  ? "release_payment".tr
                  : "offer_accept".tr),
          centerTitle: false,
        ),
        bottomNavigationBar: drawBottomBtn(
            context: context,
            text: (widget.isReleasePayment)
                ? "release_payment".tr
                : "accept_payment_methods_accept_button".tr,
            callback: () async {
              if (isCash) {
                if (widget.isReleasePayment) {
                  confirmDialog(
                    context: context,
                    title: "cash_payment".tr,
                    msg: "cash_payment_message"
                        .tr
                        .replaceAll("#name#", widget.taskBidding.ownerName),
                    callbackYes: () {
                      onCashPayment();
                    },
                  );
                } else {
                  onCashPayment();
                }
              } else {
                if (optTitle.id != null) {
                  //if (paymentId) {
                  onCardBankPayment();
                  //} else {}
                } else {
                  showToast(
                    context: context,
                    msg: "choose_payment_opt".tr,
                  );
                }
              }

              /*StripeMgr().makePayment(
                  description: "Task price",
                  amount: price.toDouble(),
                  callback: (PaymentMethod paymentMethod,
                      PaymentIntentResult paymentIntent,
                      PaymentIntentResult authPaymentIntent) {
                    showAlert(context:context,
                        msg: "Payment has been placed successfully.",
                        isToast: true,
                        which: 1);
                    Get.back();
                    /*APIViewModel().req<PaymentIntentAPIModel>(
                      context: context,
                      apiState: APIState(APIType.stripe_payment_intent_post,
                          this.runtimeType, null),
                      url: APIPaymentCfg.STRIPE_PAYMENT_INTENT_POST_URL
                          .replaceAll(
                              "#taskId#", widget.taskBidding.taskId.toString()),
                      param: {
                        'paymentMethodId': paymentMethod.id,
                        'paymentIntentId': authPaymentIntent.paymentIntentId,
                        'status': authPaymentIntent.status,
                      },
                      reqType: ReqType.Get,
                    );*/
                  });*/
              //Get.to(() => Payment());
            }),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: ListView(
        shrinkWrap: true,
        primary: true,
        children: [
          payOffer(),
        ],
      ),
    );
  }

  payOffer() {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            decoration: MyTheme.picEmboseCircleDeco,
            alignment: Alignment.center,
            child: CircleAvatar(
              radius: 30,
              backgroundColor: Colors.transparent,
              backgroundImage: new CachedNetworkImageProvider(
                  MyNetworkImage.checkUrl(widget.taskBidding.ownerImageUrl)),
            ),
          ),
          SizedBox(height: 10),
          Center(
            child: Txt(
                txt: widget.taskBidding.ownerName,
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
          ),
          SizedBox(height: 20),
          Txt(
              txt: widget.taskBidding.coverLetter,
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false),
          SizedBox(height: 20),
          drawLine(colr: MyTheme.gray3Color),
          SizedBox(height: 10),
          drawPriceBox(),
          SizedBox(height: 20),
          drawPayBox(),
          (widget.isReleasePayment) ? drawReleaseFundBox() : SizedBox(),
        ],
      ),
    );
  }

  drawPriceBox() {
    price = widget.taskBidding.netTotalAmount;
    if (price < 1) {
      price = widget.taskBidding.fixedbiddigAmount;
    }
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Flexible(
            child: Txt(
                txt: "task_price2".tr,
                txtColor: MyTheme.gray5Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.start,
                isBold: true),
          ),
          (widget.isReleasePayment && !isCash)
              ? Flexible(
                  child: Row(
                    children: [
                      Icon(Icons.lock, size: 14, color: Colors.black),
                      SizedBox(width: 5),
                      Flexible(
                        child: Txt(
                            txt: "release_payment_payment_secured".tr,
                            txtColor: MyTheme.airGreenColor,
                            txtSize: MyTheme.txtSize - .6,
                            txtAlign: TextAlign.start,
                            isBold: true),
                      )
                    ],
                  ),
                )
              : SizedBox(),
          Flexible(
            child: Text(
              getCurSign() + price.toStringAsFixed(2),
              style: TextStyle(
                color: MyTheme.gray5Color,
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
          ),
        ],
      ),
    );
  }

  drawPayBox() {
    return Container(
      color: MyTheme.gray1Color,
      child: Padding(
        padding: const EdgeInsets.only(top: 20, bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            (!widget.taskBidding.isInPersonOrOnline)
                ? Center(
                    child: ToggleSwitch(
                      minWidth: getWP(context, 30),
                      minHeight: getHP(context, MyTheme.switchBtnHpa),
                      initialLabelIndex: switchIndex,
                      cornerRadius: 50.0,
                      activeFgColor: Colors.white,
                      inactiveBgColor: HexColor.fromHex("#cad7dc"),
                      inactiveFgColor: Colors.white,
                      labels: [
                        'payment_medthod_card'.tr,
                        'payment_medthod_cash'.tr
                      ],
                      fontSize: isLng(AppDefine.GEO_CODE) ? 14 : 17,
                      //icons: [FontAwesomeIcons.mars, FontAwesomeIcons.venus],
                      activeBgColor: HexColor.fromHex("#1caade"),
                      onToggle: (index) {
                        switchIndex = index;
                        isCash = (index == 0) ? false : true;
                        setState(() {});
                      },
                    ),
                  )
                : SizedBox(),
            SizedBox(height: 20),
            (!isCash) ? drawBankCard() : drawCash(),
          ],
        ),
      ),
    );
  }

  drawBankCard() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          DropDownListDialog(
            context: context,
            title: optTitle.title,
            ddTitleList: ddTitle,
            callback: (optionItem) {
              optTitle = optionItem;
              setState(() {});
            },
          ),
          SizedBox(height: 20),
          Txt(
              txt: "offer_accepted_confirmation_funds_secured_text".tr,
              txtColor: MyTheme.gray4Color,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.center,
              isBold: false),
        ],
      ),
    );
  }

  drawCash() {
    return Container(
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Column(
          children: [
            Txt(
                txt: "offer_accepted_cash_confirmation_funds_secured_text"
                    .tr
                    .replaceAll("#name#", widget.taskBidding.ownerName),
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize,
                txtAlign: TextAlign.center,
                isBold: false),
          ],
        ),
      ),
    );
  }

  drawReleaseFundBox() {
    return Padding(
      padding: const EdgeInsets.only(top: 20),
      child: Txt(
          txt: "release_payment_terms".tr,
          txtColor: MyTheme.gray4Color,
          txtSize: MyTheme.txtSize,
          txtAlign: TextAlign.center,
          isBold: false),
    );
  }
}
