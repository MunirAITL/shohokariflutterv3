import 'dart:developer';

import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/server/APIMiscCfg.dart';
import 'package:aitl/config/server/APIPostTaskCfg.dart';
import 'package:aitl/config/server/APITypeCFg.dart';
import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/TaskInfoSearchAPIModel.dart';
import 'package:aitl/data/model/dashboard/posttask/TaskModel.dart';
import 'package:aitl/data/model/misc/device_info/UserDeviceAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/views/SoundLngBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/misc/FcmDeviceInfoHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get_utils/src/extensions/internacionalization.dart';
import 'private_msg_base.dart';

class PrivateMsgPage extends StatefulWidget {
  final String title2;
  const PrivateMsgPage({Key key, this.title2}) : super(key: key);
  @override
  State createState() => PrivateMsgPageState();
}

class PrivateMsgPageState extends BasePrivateMsgStatefull<PrivateMsgPage>
    with APIStateListener, StateListener, SingleTickerProviderStateMixin {
  List<TaskModel> listTaskModel = [];

  //  search stuff start
  //  0
  //  Sometime rebuilding whole screen might not be desirable with setState((){})
  //  for this situation you can wrap searchables with ValuelistenableBuilder widget.
  ValueNotifier<List<TaskModel>> filtered = ValueNotifier<List<TaskModel>>([]);
  FocusNode searchFocus = FocusNode();
  final searchText = TextEditingController();
  bool isSearchIconClicked = false;
  bool searching = false;
  //  search stuff end

  //  page stuff start here
  bool isPageDone = false;
  bool isRefreshing = false;
  int page = 0;
  int count = AppConfig.page_limit;

  //  tab stuff start here
  int status = TaskStatusCfg.STATUS_ALL_PRIVATEMESSAGE;

  double distance;
  double fromPrice;
  double toPrice;
  double lat;
  double lng;

  //  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_RELOAD_TAB) {
        refreshData();
      } else if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        await audioController.getAudio();
        if (audioController.isAudio.value) AudioMgr().play("private_message");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.taskinfo_search &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          final List<dynamic> locations = model.responseData.locations;
          if (locations != null) {
            //  checking to see whether page is finished to stop on reload data through API after end of scrolling for scalibility
            if (locations.length != count) {
              isPageDone = true;
            }
            try {
              for (TaskModel task in locations) {
                listTaskModel.add(task);
              }
              setState(() {});
            } catch (e) {
              myLog(e.toString());
            }
          } else {}
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  onLazyLoadAPI() async {
    if (mounted) {
      setState(() {
        isLoading = true;
      });
      await APIViewModel().req<TaskInfoSearchAPIModel>(
        context: context,
        apiState: APIState(APIType.taskinfo_search, this.runtimeType, null),
        url: APIPostTaskCfg.TASKINFOBYSEARCH_POST_URL,
        isLoading: false,
        reqType: ReqType.Get,
        param: {
          "Count": count,
          "Distance": distance ?? 0,
          "FromPrice": fromPrice ?? 20,
          "InPersonOrOnline": 0,
          "IsHideAssignTask": false,
          "Latitude": lat ?? 0.0,
          "Location": "",
          "Longitude": lng ?? 0.0,
          "Page": page,
          "SearchText": searchText.text ?? '',
          "Status": status,
          "ToPrice": toPrice ?? 2000000,
          "UserId": userData.userModel.id,
        },
      );
      if (mounted) {
        setState(() {
          isLoading = false;
        });
      }
    }
  }

  Future<void> refreshData() async {
    if (mounted) {
      setState(() {
        isSearchIconClicked = false;
        searchText.clear();
        searching = false;
        filtered.value = [];
        if (searchFocus.hasFocus) searchFocus.unfocus();
        //
        page = 0;
        isPageDone = false;
        isLoading = true;
        listTaskModel.clear();
      });
      onLazyLoadAPI();
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    listTaskModel = null;
    searchText.dispose();
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() async {
    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
    try {
      _stateProvider = new StateProvider();
      _stateProvider.subscribe(this);
    } catch (e) {}

    refreshData();
  }

  @override
  Widget build(BuildContext context) {
    String title = "private_conversation_screen_title".tr;
    try {
      /*if (myTaskController.getTaskModel() != null &&
          myTaskController.getTaskModel().title.isNotEmpty) {
        title = title + " : " + myTaskController.getTaskModel().title;
      } else {*/
      if (widget.title2 != null) {
        title = title + " : " + widget.title2;
      }
      //}
    } catch (e) {}

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          elevation: MyTheme.appbarElevation,
          //automaticallyImplyLeading: !isSearchIconClicked,
          leadingWidth: (!isSearchIconClicked) ? 0 : 56,
          leading: (!isSearchIconClicked)
              ? SizedBox()
              : IconButton(
                  onPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    isSearchIconClicked = !isSearchIconClicked;
                    searchText.clear();
                    searching = false;
                    filtered.value = [];
                    if (searchFocus.hasFocus) searchFocus.unfocus();
                    setState(() {});
                  },
                  icon: Icon(Icons.arrow_back)),
          title: (!isSearchIconClicked)
              ? UIHelper().drawAppbarTitle(title: title)
              : drawSearchbar(searchText, (text) {
                  if (text.length > 0) {
                    searching = true;
                    filtered.value = [];
                    listTaskModel.forEach((locModel) {
                      if (locModel.title
                              .toString()
                              .toLowerCase()
                              .contains(text.toLowerCase()) ||
                          locModel.ownerName
                              .toString()
                              .toLowerCase()
                              .contains(text.toLowerCase())) {
                        filtered.value.add(locModel);
                      }
                    });
                  } else {
                    searchText.clear();
                    searching = false;
                    filtered.value = [];
                    if (searchFocus.hasFocus) searchFocus.unfocus();
                  }
                }),
          centerTitle: false,
          actions: (!isSearchIconClicked)
              ? drawSoundLngBox(this.runtimeType, audioController, [
                  IconButton(
                      icon: Icon(
                        Icons.search,
                        color: Colors.white,
                        size: 30,
                      ),
                      onPressed: () {
                        isSearchIconClicked = !isSearchIconClicked;
                        setState(() {});
                      })
                ])
              : [],

          bottom: drawAppbarNavBar((index) {
            if (!isLoading) {}
          }),
        ),
        body: GestureDetector(
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: drawLayout()),
      ),
    );
  }

  drawLayout() {
    return Container(
      child: (listTaskModel.length > 0)
          ? ValueListenableBuilder<List>(
              valueListenable: filtered,
              builder: (context, value, _) {
                return RefreshIndicator(
                  color: Colors.white,
                  backgroundColor: MyTheme.brandColor,
                  onRefresh: () async {
                    isRefreshing = true;
                    refreshData();
                    return;
                  },
                  notificationPredicate: (scrollNotification) {
                    if (scrollNotification is ScrollStartNotification) {
                      //print('Widget has started scrolling');
                    } else if (scrollNotification is ScrollEndNotification) {
                      Future.delayed(Duration(seconds: 1), () {
                        if (!isRefreshing && !isPageDone) {
                          page++;
                          onLazyLoadAPI();
                        }
                      });
                    }
                    return true;
                  },
                  child: ListView.builder(
                    addAutomaticKeepAlives: true,
                    cacheExtent: AppConfig.page_limit.toDouble(),
                    scrollDirection: Axis.vertical,
                    shrinkWrap: true,
                    //primary: false,
                    itemCount: searching
                        ? filtered.value.length
                        : listTaskModel.length,
                    itemBuilder: (BuildContext context, int index) {
                      return drawItem(searching
                          ? filtered.value[index]
                          : listTaskModel[index]);
                    },
                  ),
                );
              },
            )
          : (!isLoading)
              ? drawNF()
              : Container(),
    );
  }
}
