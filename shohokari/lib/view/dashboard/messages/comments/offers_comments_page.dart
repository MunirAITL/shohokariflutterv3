import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:aitl/config/app/status/TaskStatusCfg.dart';
import 'package:aitl/config/cfg/AppConfig.dart';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/server/APIEmailNotiCfg.dart';
import 'package:aitl/config/server/APITimelineCfg.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/app_data/UserData.dart';
import 'package:aitl/data/model/dashboard/mytasks/biddings/TaskBiddingModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/CommentAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/GetCommentsAPIModel.dart';
import 'package:aitl/data/model/dashboard/timeline/comments/UserCommentPublicModelList.dart';
import 'package:aitl/data/model/dashboard/timeline/pubnub/ChatModel.dart';
import 'package:aitl/data/model/dashboard/user/PublicUserModel.dart';
import 'package:aitl/data/model/misc/CommonAPIModel.dart';
import 'package:aitl/data/model/misc/media_upload/MediaUploadFilesAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/pubnub/PubnubMgr.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'offers_comments_base.dart';

class OffersCommentsPage extends StatefulWidget {
  final TaskBiddingModel taskBiddingsModel;
  final String description, title, body;
  const OffersCommentsPage({
    Key key,
    @required this.taskBiddingsModel,
    this.description,
    this.title,
    this.body,
  }) : super(key: key);
  @override
  State createState() => _OffersChatPageState();
}

class _OffersChatPageState extends BaseOffersStatefull<OffersCommentsPage>
    with APIStateListener, StateListener {
  final TextEditingController textController = TextEditingController();
  ScrollController scrollController = new ScrollController();

  List<UserCommentPublicModelList> listCommentsModel = [];

  PubnubMgr pubnubMgr;

  //  page stuff start here
  bool isPageDone = false;
  bool isScrolling = false;
  bool isPosting = false;
  int pageStart = 1;
  int pageCount = AppConfig.page_limit;
  String msgTmp = '';

//  **************  app states start

  @override
  void onDetached() {
    try {
      myLog("app state = onDetached");
    } catch (e) {}
  }

  @override
  void onInactive() {
    try {
      myLog("app state = onInactive");
    } catch (e) {}
  }

  @override
  void onPaused() {
    try {
      myLog("app state = onPaused");
    } catch (e) {}
  }

  @override
  void onResumed() {
    try {
      myLog("app state = onResumed");
    } catch (e) {}
  }

  //  **************  app states end

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START) {
        await audioController.getAudio();
        if (audioController.isAudio.value) AudioMgr().play("review_offers");
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  APIStateProvider _apiStateProvider;
  @override
  void onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.get_comments &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              final listCommentsModel2 =
                  (model as GetCommentsAPIModel).comments;
              if (listCommentsModel2.length > 0 &&
                  listCommentsModel2.length != listCommentsModel.length) {
                listCommentsModel = listCommentsModel2;
                setState(() {
                  if (listCommentsModel.length < pageCount) {
                    pageStart = 1;
                    isPageDone = true;
                  }

                  if (!isScrolling) {
                    Future.delayed(const Duration(seconds: 1), () {
                      scrollDown(scrollController,
                          getH(context) * AppConfig.chatScrollHeight);
                    });
                  }
                });
              }
            } catch (e) {
              myLog(e.toString());
            }
          }
        }
      } else if (apiState.type == APIType.media_upload_file &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsPostOffersComment(
                (model as MediaUploadFilesAPIModel).responseData.images[0].url);
          }
        }
      } else if (apiState.type == APIType.post_comment &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            wsGetComments();
            final commentModel = (model as CommentAPIModel).comment;
            try {
              final user = new User(
                  id: userData.userModel.id, name: userData.userModel.name);
              final chatModel =
                  new ChatModel(commentModel: commentModel, user: user);
              pubnubMgr.postMessage(
                  chatModel: chatModel, receiverId: commentModel.user.id);
            } catch (e) {}

            APIViewModel().req<CommonAPIModel>(
              context: context,
              apiState: APIState(APIType.email_noti, this.runtimeType, null),
              url: APIEmailNotiCfg.COMMENTS_EMAI_NOTI_GET_URL.replaceAll(
                  "#commentId#",
                  (model as CommentAPIModel).comment.id.toString()),
              reqType: ReqType.Get,
              isLoading: false,
            );
          }
        }
      } else if (apiState.type == APIType.email_noti &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {} catch (e) {}
          }
        }
      }
    } catch (e) {}
  }

  wsGetTimelineByApp() {}
  wsWithdrawTaskbidding() {}
  onDelTaskClicked() {}

  wsPostOffersComment(String url) async {
    try {
      isPosting = true;
      final param = {
        "AdditionalData": url,
        "CanDelete": true,
        "CommentText": msgTmp,
        "EntityId": widget.taskBiddingsModel.id,
        "EntityName": "TaskBidding",
        "UserId": userData.userModel.id,
      };
      myLog(json.encode(param));
      msgTmp = '';
      await APIViewModel().req<CommentAPIModel>(
        context: context,
        apiState: APIState(APIType.post_comment, this.runtimeType, null),
        url: APITimelineCfg.COMMENTS_POST_URL,
        param: param,
        reqType: ReqType.Post,
      );
      isPosting = false;
    } catch (e) {}
  }

  wsGetComments() async {
    try {
      await APIViewModel().req<GetCommentsAPIModel>(
        context: context,
        apiState: APIState(APIType.get_comments, this.runtimeType, null),
        url: APITimelineCfg.GET_COMMENTS_URL,
        param: {
          "Count": pageCount,
          "EntityId": widget.taskBiddingsModel.id,
          "EntityName": "TaskBidding",
          "Page": pageStart,
        },
        reqType: ReqType.Get,
        isLoading: false,
      );
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  @override
  void dispose() {
    listCommentsModel = null;
    textController.dispose();
    scrollController.dispose();
    scrollController = null;
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
      _stateProvider.unsubscribe(this);
      _stateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      pubnubMgr.destroy();
    } catch (e) {}
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  @override
  getRefreshData() {}

  appInit() async {
    try {
      try {
        _apiStateProvider = new APIStateProvider();
        _apiStateProvider.subscribe(this);
      } catch (e) {}

      try {
        _stateProvider = new StateProvider();
        _stateProvider.subscribe(this);
        StateProvider()
            .notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
      } catch (e) {}

      await wsGetComments();
      pubnubMgr = PubnubMgr();
      await pubnubMgr.initPubNub(callback: (envelope) async {
        myLog(envelope.payload);
        if (!isPosting) {
          pageStart = 1;
          await wsGetComments();
        }
      });
    } catch (e) {
      myLog(e.toString());
    }
  }

  Widget buildMessageList() {
    return Flexible(
      child: NotificationListener(
        onNotification: (scrollNotification) {
          if (scrollNotification is ScrollStartNotification) {
            isScrolling = true;
            if (!isPageDone && msgTmp == '') {
              pageStart++;
              wsGetComments();
            }
          } else if (scrollNotification is ScrollEndNotification) {
            isScrolling = false;
          }
          return true;
        },
        child: ListView.builder(
          controller: scrollController,
          //padding: new EdgeInsets.all(8.0),
          reverse: false,
          //shrinkWrap: true,
          itemCount: listCommentsModel.length,
          itemBuilder: (BuildContext context, int index) {
            return buildSingleMessage(listCommentsModel, index);
          },
        ),
      ),
    );
  }

  _chatTextArea() {
    if (myTaskController.getStatusCode() ==
        TaskStatusCfg.TASK_STATUS_PAYMENTED) {
      return Container(
          color: MyTheme.hotdipPink.withAlpha(60),
          width: getW(context),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Txt(
                txt: "task_paid_offer_closed".tr,
                txtColor: MyTheme.brandColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false),
          ));
    } else if (myTaskController.getStatusCode() ==
        TaskStatusCfg.TASK_STATUS_CANCELLED) {
      return Container(
          color: MyTheme.hotdipPink.withAlpha(60),
          width: getW(context),
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: Txt(
                txt: "task_cancel_offer_closed".tr,
                txtColor: MyTheme.brandColor,
                txtSize: MyTheme.txtSize - .2,
                txtAlign: TextAlign.center,
                isBold: false),
          ));
      ;
    } else {
      UserCommentPublicModelList commentsModel;
      try {
        commentsModel = listCommentsModel[index2];
      } catch (e) {}
      final name = widget.taskBiddingsModel.ownerName ?? '';
      return Padding(
        padding: const EdgeInsets.only(bottom: 10, top: 10),
        child: drawMessageBox(
          context: context,
          picUrl: userData.userModel.profileImageUrl,
          hintText: 'reply_to'.tr.replaceAll("#name#", name),
          textController: textController,
          callbackCam: (File file, String txt) async {
            textController.clear();
            msgTmp = txt.trim();
            if (file != null) {
              await APIViewModel().upload(
                context: context,
                apiState:
                    APIState(APIType.media_upload_file, this.runtimeType, null),
                file: file,
              );
            } else {
              FocusScope.of(context).requestFocus(FocusNode());
              if (txt.trim().isNotEmpty) {
                //Add the message to the list
                if (commentsModel != null) {
                  String formattedDate =
                      DateFormat('dd-MMM-yyyy').format(DateTime.now());
                  final UserCommentPublicModelList timeLinePostModel =
                      UserCommentPublicModelList();
                  final user = timeLinePostModel.user = User();
                  user.id = commentsModel.user.id;
                  user.profileImageUrl = commentsModel.user.profileImageUrl;
                  timeLinePostModel.user = user;
                  timeLinePostModel.additionalData = '';
                  timeLinePostModel.commentText = msgTmp;
                  timeLinePostModel.dateCreatedUtc = formattedDate;
                  listCommentsModel.add(timeLinePostModel);
                  scrollDown(scrollController,
                      getH(context) * AppConfig.chatScrollHeight);
                }
                textController.clear();
                setState(() {});
                Future.delayed(Duration(seconds: 1), () async {
                  pageStart = 1;
                  wsPostOffersComment(null);
                });
              }
            }
          },
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: MyTheme.appbarElevation,
          backgroundColor: MyTheme.bgColor,
          iconTheme: MyTheme.themeData.iconTheme,
          title: UIHelper().drawAppbarTitle(title: 'offer_chat'.tr),
          centerTitle: false,
        ),
        body: GestureDetector(
          onTap: () {
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: drawLayout(),
        ),
      ),
    );
  }

  drawLayout() {
    return Container(
      //width: double.infinity,
      // height: double.infinity,
      child: Column(
        children: <Widget>[
          Padding(
              padding: const EdgeInsets.only(left: 10, right: 10),
              child: drawOffers([widget.taskBiddingsModel], true)),
          buildMessageList(),
          _chatTextArea(),
        ],
      ),
    );
  }
}
