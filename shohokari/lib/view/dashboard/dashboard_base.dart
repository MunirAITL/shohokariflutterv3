import 'package:aitl/config/server/APIProfileCfg.dart';
import 'package:aitl/data/app_data/PrefMgr.dart';
import 'package:aitl/data/model/dashboard/more/profile/PublicProfileAPIModel.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/utils/APIHelper.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/rx/AudioController.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class BaseDashboard<T extends StatefulWidget> extends State<T>
    with Mixin, UIHelper, WidgetsBindingObserver {
  final audioController = Get.put(AudioController());

  var indexLng = 0;
  drawLayout();

  wsUserDevice({String eventType = ''}) async {
    try {
      await APIHelper().wsUserDevice(
          context: context, eventType: eventType, callback: (model) {});
    } catch (e) {}
  }

  wsUserProfile({int userId, Function(PublicProfileAPIModel) callback}) async {
    await APIViewModel().req<PublicProfileAPIModel>(
        context: context,
        url: APIProfileCFg.PUBLIC_USER_GET_URL
            .replaceAll("#userId#", userId.toString()),
        reqType: ReqType.Get,
        callback: (model) {
          callback(model);
        });
  }

  @override
  Widget build(BuildContext context) {
    return null;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    initPage();
  }

  @override
  void dispose() {
    //WidgetsBinding.instance.removeObserver(this);
    //audioController.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        onResumed();
        break;
      case AppLifecycleState.inactive:
        onPaused();
        break;
      case AppLifecycleState.paused:
        onInactive();
        break;
      case AppLifecycleState.detached:
        onDetached();
        break;
    }
  }

  void onResumed();
  void onPaused();
  void onInactive();
  void onDetached();

  initPage() async {
    indexLng = await PrefMgr.shared.getPrefInt("local");
  }
}
