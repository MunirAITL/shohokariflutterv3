import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'InputBoxHT.dart';

InputBox({
  ctrl,
  context,
  lableTxt,
  kbType,
  inputAction,
  focusNode,
  focusNodeNext,
  len,
  isPwd = false,
  autofocus = false,
  labelColor = Colors.black,
  align = TextAlign.left,
  isShowHint = false,
  isFirstLetterCap = false,
  prefixIcon,
  suffixIcon,
  ecap = eCap.None,
}) =>
    TextField(
      controller: ctrl,
      focusNode: focusNode,
      autofocus: autofocus,
      keyboardType: kbType,
      textInputAction: inputAction,
      onEditingComplete: () {
        // Move the focus to the next node explicitly.
        if (focusNode != null) {
          focusNode.unfocus();
        } else {
          FocusScope.of(context).requestFocus(new FocusNode());
        }
        if (focusNodeNext != null) {
          FocusScope.of(context).requestFocus(focusNodeNext);
        } else {
          FocusScope.of(context).requestFocus(new FocusNode());
        }
      },
      inputFormatters: (kbType == TextInputType.phone)
          ? <TextInputFormatter>[FilteringTextInputFormatter.digitsOnly]
          : (kbType == TextInputType.emailAddress)
              ? [
                  FilteringTextInputFormatter.allow(RegExp(
                      "^[a-zA-Z0-9_.+-]*(@([a-zA-Z0-9-.]*(\\.[a-zA-Z0-9-]*)?)?)?")),
                ]
              : (ecap != eCap.None)
                  ? [
                      (ecap == eCap.All)
                          ? AllUpperCaseTextFormatter()
                          : (ecap == eCap.Sentence)
                              ? FirstUpperCaseTextFormatter()
                              : WordsUpperCaseTextFormatter()
                    ]
                  : null,
      obscureText: isPwd,
      maxLength: len,
      autocorrect: false,
      enableSuggestions: false,
      textAlign: align,
      style: TextStyle(
        color: Colors.black,
        fontSize: 17,
        height: MyTheme.txtLineSpace,
      ),
      decoration: new InputDecoration(
        counter: Offstage(),
        hintText: (isShowHint) ? lableTxt : '',
        labelText: (!isShowHint) ? lableTxt : '',
        prefixIcon: (prefixIcon != null) ? prefixIcon : null,
        suffixIcon: (suffixIcon != null) ? suffixIcon : null,
        hintStyle: new TextStyle(
          color: labelColor,
          fontSize: 17,
          height: MyTheme.txtLineSpace,
        ),
        labelStyle: new TextStyle(
          color: labelColor,
          fontSize: 17,
          height: MyTheme.txtLineSpace,
        ),
        //contentPadding: EdgeInsets.only(left: 20, right: 20),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: labelColor,
            width: 1,
          ),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: MyTheme.blueColor,
            width: 1,
          ),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.black,
            width: 1,
          ),
        ),
      ),
    );

class _UpperCaseTextFormatter extends TextInputFormatter {
  @override
  TextEditingValue formatEditUpdate(
      TextEditingValue oldValue, TextEditingValue newValue) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}
