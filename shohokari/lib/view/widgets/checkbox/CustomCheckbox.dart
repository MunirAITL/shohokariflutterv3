import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/mixin.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:flutter/material.dart';

class CustomCheckbox extends StatelessWidget with Mixin {
  final String txt;
  ThemeData themeData;
  final bool isSelected;
  final Function(bool) callback;

  CustomCheckbox({
    Key key,
    @required this.txt,
    this.themeData,
    @required this.callback,
    @required this.isSelected,
  }) {
    if (this.themeData == null) {
      themeData = MyTheme.radioThemeData;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => callback(!isSelected),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        //crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Theme(
            data: themeData,
            child: Checkbox(
              value: isSelected,
              visualDensity: VisualDensity.compact,
              onChanged: (val) => callback(val),
            ),
          ),
          SizedBox(width: 10),
          Flexible(
            child: Txt(
              txt: txt,
              txtColor: Colors.black,
              txtSize: MyTheme.txtSize,
              txtAlign: TextAlign.start,
              isBold: false,
            ),
          ),
        ],
      ),
    );
    /*GestureDetector(
      onTap: () {
        callback(!isSelected);
      },
      child: Container(
        color: Colors.red,
        width: getWP(context, 90),
        child: Theme(
          data: themeData,
          child: Transform.translate(
            offset: Offset(-10, 0),
            child: Row(
              //mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Transform.scale(
                  scale: 1.2,
                  child: Checkbox(
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                    value: isSelected,
                    onChanged:
                        null, /*(newValue) {
                      callback(newValue);
                    },*/
                  ),
                ),
                //SizedBox(width: 10),
                Expanded(
                  //flex: 2,
                  child: Txt(
                      txt: txt,
                      txtColor: Colors.black,
                      txtSize: MyTheme.txtSize,
                      txtAlign: TextAlign.start,
                      isBold: false),
                ),
              ],
            ),
          ),
        ),
      ),
    );*/
  }
}
