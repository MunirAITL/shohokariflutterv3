import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/data/translations/LanguageTranslations.dart';
import 'package:aitl/view/widgets/btn/IconBtnQ.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/AudioController.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

drawSoundLngBox(
    Type cls, AudioController audioController, List<Widget> listIcons) {
  bool isQiconFound = false;
  List<Widget> listW = [];
  audioController.getAudio();

  for (var w in listIcons) {
    try {
      final o = w as IconBtnQ;
      if (o.tag == 1) {
        isQiconFound = true;
        listW.add(GestureDetector(
          onTap: () {
            audioController.setAudio(!audioController.isAudio.value);
            if (audioController.isAudio.value)
              StateProvider().notify(ObserverState.STATE_AUDIO_START, cls);
            else
              StateProvider().notify(ObserverState.STATE_AUDIO_STOP, cls);
          },
          child: Obx(() => SvgPicture.asset(
                "assets/images/svg/ic_sound_" +
                    ((audioController.isAudio.value) ? 'on' : 'off') +
                    ".svg",
                color: Colors.white,
              )),
        ));
        listW.add(TextButton(
            onPressed: () async {
              LngTR().setLng();
              StateProvider().notify(ObserverState.STATE_RELOAD_SOUND, null);
            },
            child: Txt(
              txt: LngTR().getLngTitle(),
              txtColor: Colors.white,
              txtSize: MyTheme.txtSize - .2,
              txtAlign: TextAlign.start,
              isBold: false,
            )));
      }
      listW.add(w);
    } catch (e) {
      listW.add(w);
    }
  }

  if (!isQiconFound) {
    listW.add(GestureDetector(
      onTap: () {
        audioController.setAudio(!audioController.isAudio.value);
        if (audioController.isAudio.value)
          StateProvider().notify(ObserverState.STATE_AUDIO_START, cls);
        else
          StateProvider().notify(ObserverState.STATE_AUDIO_STOP, cls);
      },
      child: Obx(() => SvgPicture.asset(
            "assets/images/svg/ic_sound_" +
                ((audioController.isAudio.value) ? 'on' : 'off') +
                ".svg",
            color: Colors.white,
          )),
    ));
    listW.add(TextButton(
        onPressed: () async {
          LngTR().setLng();
          StateProvider().notify(ObserverState.STATE_RELOAD_SOUND, null);
        },
        child: Txt(
          txt: LngTR().getLngTitle(),
          txtColor: Colors.white,
          txtSize: MyTheme.txtSize - .2,
          txtAlign: TextAlign.start,
          isBold: false,
        )));
  }

  return listW;
}
