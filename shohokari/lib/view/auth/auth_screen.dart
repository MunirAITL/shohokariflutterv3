import 'dart:ui';
import 'package:aitl/config/cfg/AppDefine.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/data/translations/LanguageTranslations.dart';
import 'package:aitl/view/auth/login_page.dart';
import 'package:aitl/view/auth/reg_page1.dart';
import 'package:aitl/view/widgets/views/SoundLngBox.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view_model/helper/utils/UIHelper.dart';
import 'package:aitl/view_model/observer/StateProvider.dart';
import 'package:aitl/view_model/rx/AudioController.dart';
import 'package:flutter/material.dart';
import 'package:aitl/mixin.dart';
import 'package:get/get.dart';

class AuthScreen extends StatefulWidget {
  bool isSwitch = true;
  String fname = '';
  String lname = '';
  String email = '';
  String mobile = '';
  String pwd = '';
  String compName = '';
  int initialIndex;
  AuthScreen({this.initialIndex = 1});
  @override
  State createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen>
    with Mixin, SingleTickerProviderStateMixin, StateListener {
  final audioController = Get.put(AudioController());
  TabController tabController;

  var tabOrder = 1;

  StateProvider _stateProvider;
  @override
  void onStateChanged(state, data) async {
    try {
      if (state == ObserverState.STATE_AUDIO_START &&
          data == this.runtimeType) {
        //final audioController = Get.put(AudioController());
        await audioController.getAudio();
        if (audioController.isAudio.value) {
          await AudioMgr().play((tabOrder == 0) ? "create_profile" : "login");
        }
      } else if (state == ObserverState.STATE_AUDIO_STOP) {
        AudioMgr().stop();
      }
    } catch (e) {}
  }

  @override
  void initState() {
    super.initState();

    tabOrder = widget.initialIndex;
    _stateProvider = new StateProvider();
    _stateProvider.subscribe(this);
    StateProvider().notify(ObserverState.STATE_AUDIO_START, this.runtimeType);
    tabController = TabController(vsync: this, length: 2)
      ..addListener(() async {
        if (!tabController.indexIsChanging) {
          print(tabController.index);
          tabOrder = tabController.index;
          await audioController.getAudio();
          if (audioController.isAudio.value) {
            AudioMgr().play((tabOrder == 0) ? "create_profile" : "login");
            setState(() {});
          }
        }
      });
    tabController.index = 1;
  }

  //@mustCallSuper
  @override
  void dispose() {
    _stateProvider.unsubscribe(this);
    _stateProvider = null;
    try {
      audioController.dispose();
    } catch (e) {}
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      initialIndex: widget.initialIndex,
      child: SafeArea(
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            backgroundColor: MyTheme.bgColor,
            iconTheme: MyTheme.themeData.iconTheme,
            elevation: MyTheme.appbarElevation,
            /*leading: IconButton(
                icon: Icon(Icons.arrow_back),
                onPressed: () {
                  Get.back();
                }),*/
            title: UIHelper().drawAppbarTitle(title: 'join_shohokari_title'.tr),
            centerTitle: false,
            actions: drawSoundLngBox(this.runtimeType, audioController, []),
            bottom: PreferredSize(
              preferredSize: Size.fromHeight(getHP(context, 7)),
              child: Container(
                //color: MyTheme.redColor,
                height: getHP(context, MyTheme.btnHpa),
                child: TabBar(
                  controller: tabController,
                  //labelColor: Colors.deepOrange,
                  unselectedLabelColor: Colors.grey,
                  indicatorColor: Colors.white,
                  indicatorSize: TabBarIndicatorSize.tab,
                  indicatorWeight: 3,
                  tabs: [
                    Container(
                        //color: MyTheme.blueColor,
                        //height: getHP(context, MyTheme.btnHpa),
                        child: Txt(
                      txt: "sign_up_create_account".tr,
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    )),
                    Container(
                        child: Txt(
                      txt: "log_in".tr,
                      txtColor: Colors.white,
                      txtSize: MyTheme.txtSize - .2,
                      txtAlign: TextAlign.center,
                      isBold: false,
                    )),
                  ],
                ),
              ),
            ),
          ),
          body: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onPanDown: (detail) {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            onTap: () {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            child: TabBarView(
              controller: tabController,
              children: [
                RegView(),
                LoginView(),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
