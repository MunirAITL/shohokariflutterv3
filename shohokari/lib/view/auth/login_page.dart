import 'package:aitl/config/server/APIAuthCfg.dart';
import 'package:aitl/config/server/Server.dart';
import 'package:aitl/config/theme/MyTheme.dart';
import 'package:aitl/controller/audio/audio_mgr.dart';
import 'package:aitl/controller/form_validator/UserProfileVal.dart';
import 'package:aitl/data/model/auth/ForgotAPIModel.dart';
import 'package:aitl/data/model/auth/LoginAPIModel.dart';
import 'package:aitl/data/network/ModelMgr.dart';
import 'package:aitl/data/network/NetworkMgr.dart';
import 'package:aitl/view/widgets/btn/MMBtn.dart';
import 'package:aitl/view/widgets/dialog/ConfirmDialog.dart';
import 'package:aitl/view/widgets/txt/Txt.dart';
import 'package:aitl/view/widgets/input/InputBox.dart';
import 'package:aitl/view_model/api/api_view_model.dart';
import 'package:aitl/view_model/helper/auth/AuthHelper.dart';
import 'package:aitl/view_model/helper/auth/LoginHelper.dart';
import 'package:aitl/view_model/observer/APIStateProvider.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'auth_base.dart';
import 'otp/sms_page2.dart';

class LoginView extends StatefulWidget {
  @override
  State createState() => _LoginViewState();
}

class _LoginViewState extends BaseAuth<LoginView> with APIStateListener {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _pwd = TextEditingController();
  final focusEmail = FocusNode();
  final focusPwd = FocusNode();

  APIStateProvider _apiStateProvider;
  @override
  onAPIStateChanged(apiState, model) async {
    try {
      if (apiState.type == APIType.login && apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel);
        }
      } else if (apiState.type == APIType.forgot &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          if (model.success) {
            try {
              final msg = model.messages.forgotpassword[0].toString();
              showToast(context: context, msg: msg, which: 1);
            } catch (e) {
              myLog(e.toString());
            }
          } else {
            try {
              final err = model.errorMessages.forgotpassword[0].toString();
              showToast(context: context, msg: err);
            } catch (e) {
              myLog(e.toString());
            }
          }
        }
      } else if (apiState.type == APIType.loginWithG &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel);
        }
      } else if (apiState.type == APIType.loginWithFB &&
          apiState.cls == this.runtimeType) {
        if (model != null && mounted) {
          checkLoginRes(model as LoginAPIModel);
        }
      }
    } catch (e) {
      myLog(e.toString());
    }
  }

  @override
  void initState() {
    super.initState();
    appInit();
  }

  //@mustCallSuper
  @override
  void dispose() {
    _email.dispose();
    _pwd.dispose();
    try {
      _apiStateProvider.unsubscribe(this);
      _apiStateProvider = null;
    } catch (e) {
      myLog(e.toString());
    }
    try {
      NetworkMgr().dispose();
    } catch (e) {}
    try {
      AudioMgr().stop2();
    } catch (e) {}
    super.dispose();
  }

  appInit() {
    try {
      if (!Server.isOtp) {
        //_email.text =
        //"nipun@aitl.net"; //"occbs.cust0081@yopmail.com"; //"tah12@yopmail.com";
        //_pwd.text = "E6jm*&Hu**\$\$!!!";

        _email.text = "rahmanrichie7@gmail.com";
        _pwd.text = "richei\$25";
      }
    } catch (e) {}

    try {
      _apiStateProvider = new APIStateProvider();
      _apiStateProvider.subscribe(this);
    } catch (e) {}
  }

  validate() {
    if (UserProfileVal().isEmpty(context, _email, 'missing_ep'.tr)) {
      return false;
    } else if (!UserProfileVal().isPwdOK(context, _pwd)) {
      return false;
    }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              SizedBox(height: 30),
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(left: 40, right: 40),
                  child: Column(
                    children: [
                      InputBox(
                        context: context,
                        ctrl: _email,
                        lableTxt: "email_or_phone_number".tr,
                        kbType: TextInputType.emailAddress,
                        inputAction: TextInputAction.next,
                        focusNode: focusEmail,
                        focusNodeNext: focusPwd,
                        len: 50,
                      ),
                      SizedBox(height: 10),
                      InputBox(
                        context: context,
                        ctrl: _pwd,
                        lableTxt: "password".tr,
                        kbType: TextInputType.text,
                        inputAction: TextInputAction.done,
                        focusNode: focusPwd,
                        len: 20,
                        isPwd: true,
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          GestureDetector(
                              onTap: () {
                                confirmDialog(
                                  context: context,
                                  title: "dialog_password_reset_title".tr,
                                  msg: "forgot_pwd_txt1".tr +
                                      _email.text.trim() +
                                      "forgot_pwd_txt2".tr,
                                  callbackYes: () {
                                    if (_email.text.trim().length > 0) {
                                      APIViewModel().req<ForgotAPIModel>(
                                        context: context,
                                        apiState: APIState(APIType.forgot,
                                            this.runtimeType, null),
                                        url: APIAuthCfg.FORGOT_URL,
                                        param: {'email': _email.text.trim()},
                                        reqType: ReqType.Post,
                                      );
                                    } else {
                                      showToast(
                                        context: context,
                                        msg: "missing_email".tr,
                                      );
                                    }
                                  },
                                );
                              },
                              child: Txt(
                                  txt:
                                      "sign_up_login_button_forgot_password".tr,
                                  txtColor: MyTheme.blueColor,
                                  txtSize: MyTheme.txtSize,
                                  txtAlign: TextAlign.center,
                                  isBold: false)),
                          SizedBox(width: 10),
                          MMBtn(
                            txt: "introduction_screen_login_button".tr,
                            //txtColor: Colors.white,
                            //bgColor: accentColor,
                            width: getWP(context, 30),
                            height: getHP(context, MyTheme.btnHpa),
                            radius: 20,
                            callback: () {
                              if (validate()) {
                                APIViewModel().req<LoginAPIModel>(
                                  context: context,
                                  apiState: APIState(
                                      APIType.login, this.runtimeType, null),
                                  url: APIAuthCfg.LOGIN_URL,
                                  param: LoginHelper().getParam(
                                      email: _email.text.trim(),
                                      pwd: _pwd.text.trim()),
                                  reqType: ReqType.Post,
                                  isCookie: true,
                                );
                              }
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20),
              Txt(
                txt: "sign_up_login_separator_or".tr,
                txtColor: MyTheme.gray4Color,
                txtSize: MyTheme.txtSize + .3,
                txtAlign: TextAlign.center,
                isBold: false,
              ),
              Padding(
                padding: const EdgeInsets.only(left: 40, right: 40, top: 20),
                child: MMBtn(
                  txt: "sign_up_login_main_button_label_mobile_login".tr,
                  width: getW(context),
                  height: getHP(context, MyTheme.btnHpa),
                  radius: 10,
                  callback: () async {
                    Get.to(
                      () => Sms2Page(),
                    ).then((value) {
                      //callback(route);
                    });
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20, left: 40, right: 40),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    /*AuthHelper().drawGoogleFBLoginButtons(
                      context: context,
                      height: getHP(context, MyTheme.btnHpa),
                      width: getW(context),
                      icon: "ic_fb",
                      txt: "com_facebook_loginview_log_in_button_continue".tr,
                      callback: () {
                        loginWithFB(this.runtimeType);
                      },
                    ),
                    SizedBox(height: 10),*/
                    AuthHelper().drawGoogleFBLoginButtons(
                      context: context,
                      height: getHP(context, MyTheme.btnHpa),
                      width: getW(context),
                      icon: "ic_google",
                      txt: "com_google_loginview_log_in_button_continue".tr,
                      callback: () {
                        loginWithGoogle(this.runtimeType);
                      },
                    ),
                  ],
                ),
              ),
              SizedBox(height: 50),
            ],
          ),
        ),
      ),
    );
  }
}
