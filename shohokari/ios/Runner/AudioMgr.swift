//
//  AudioMgr.swift
//  Runner
//
//  Created by aitl on 10/20/21.
//

import Foundation
import AVFoundation
import Flutter

class AudioMgr {
     
    var player = AVAudioPlayer()
    
    func start(call: FlutterMethodCall, result: @escaping FlutterResult) {
        if let args = call.arguments as? Dictionary<String, Any>,
            let file = args["file"] as? String {
            let path = Bundle.main.path(forResource:"raw/"+file, ofType: "mp3")
            //let url = Bundle.main.url(forResource: "audio/"+file, withExtension: "mp3")
            do{
                print(path!)
                stop()
                player = try AVAudioPlayer(contentsOf:  URL.init(string: path!)!)
                player.prepareToPlay()
                player.play()
                print("************************************** audio started")
            } catch {
                print(file + ": File is not Loaded")
            }
        }
    }
    
    func stop() {
        
        if player.isPlaying {
            print("audio stopping **************************************")
            player.stop()
        }
        
    }
    
}
