package com.aitl.shohokari

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import com.sslcommerz.library.payment.Classes.PayUsingSSLCommerz
import com.sslcommerz.library.payment.Listener.OnPaymentResultListener
import com.sslcommerz.library.payment.Util.ConstantData.CurrencyType
import com.sslcommerz.library.payment.Util.ConstantData.ErrorKeys
import com.sslcommerz.library.payment.Util.ConstantData.SdkCategory
import com.sslcommerz.library.payment.Util.ConstantData.SdkType
import com.sslcommerz.library.payment.Util.JsonModel.TransactionInfo
import com.sslcommerz.library.payment.Util.Model.AdditionalFieldModel
import com.sslcommerz.library.payment.Util.Model.CustomerFieldModel
import com.sslcommerz.library.payment.Util.Model.MandatoryFieldModel
import com.sslcommerz.library.payment.Util.Model.ShippingFieldModel
import io.flutter.plugin.common.MethodCall

class PaymentAPIActivity {
    companion object {
        internal val TAG = "PaymentAPIActivity"
    }

    fun recommendationsData(callback: (String) -> Unit) {
        callback("munir testing")
    }

    public fun paymentCardBySSLCommerzx(context: Context, call: MethodCall, callback: (String) -> Unit) {

        var fName = call.argument<String>("fName")
        var lName = call.argument<String>("lName")
        var email = call.argument<String>("email")
        var mobile = call.argument<String>("mobile")
        var netTotalAmount = call.argument<Double>("netTotalAmount")
        var taskbiddingId = call.argument<Int>("taskbiddingId")
        var cardType = call.argument<String>("cardType")

        //val mandatoryFieldModel = MandatoryFieldModel("shohokarilive", "5AF2F531375BB64072", netTotalAmount.toString(), taskbiddingId.toString(), CurrencyType.BDT, SdkType.LIVE, SdkCategory.BANK_PAGE, cardType)
        val mandatoryFieldModel = MandatoryFieldModel("aitl5a969337b079b", "aitl5a969337b079b@ssl", netTotalAmount.toString(), taskbiddingId.toString(), CurrencyType.BDT, SdkType.TESTBOX, SdkCategory.BANK_PAGE, cardType)
        /*Optional Fields*/
        val customerFieldModel = CustomerFieldModel(fName + " " + lName, email, "", "", "", "", "", "", mobile, "")
        val shippingFieldModel = ShippingFieldModel("", "", "", "", "", "", "")
        val additionalFieldModel = AdditionalFieldModel()
        //        additionalFieldModel.setValueA("Value Option 1")
        //        additionalFieldModel.setValueB("Value Option 1")
        //        additionalFieldModel.setValueC("Value Option 1")
        //        additionalFieldModel.setValueD("Value Option 1")
        /*Call for the payment*/
        PayUsingSSLCommerz.getInstance().setData(context, mandatoryFieldModel, customerFieldModel, shippingFieldModel, additionalFieldModel, object : OnPaymentResultListener {
            override fun transactionSuccess(transactionInfo: TransactionInfo) {
                // If payment is success and risk label is 0.
                if (transactionInfo.getRiskLevel().equals("0")) {
                    Log.d(PaymentAPIActivity.TAG, "Transaction Successfully completed")
                    Toast.makeText(context, "Transaction Successfully completed", Toast.LENGTH_LONG)

                    var gson = Gson()
                    var objectString = gson.toJson(transactionInfo)
                    print(objectString)

                    callback.invoke(objectString)

                } else {
                    //Log.d(ReleasePaymentActivity.TAG, "Transaction in risk. Risk Title : " + transactionInfo.getRiskTitle().toString())
                    Toast.makeText(context, "Transaction in risk. Risk Title : " + transactionInfo.getRiskTitle().toString(), Toast.LENGTH_LONG)
                    //callback(transactionInfo.getRiskTitle().toString())
                }// Payment is success but payment is not complete yet. Card on hold now.
                //  finish()
            }

            override fun transactionFail(transactionInfo: TransactionInfo) {
                // Transaction failed
                Log.e(PaymentAPIActivity.TAG, "Transaction Fail")
                Toast.makeText(context, "Transaction Fail", Toast.LENGTH_LONG)
                //callback(transactionInfo.getRiskTitle().toString())
            }

            override fun error(errorCode: Int) {
                when (errorCode) {
                    // Your provides information is not valid.
                    ErrorKeys.USER_INPUT_ERROR -> {
                        Log.e(PaymentAPIActivity.TAG, "User Input Error")
                        Toast.makeText(context, "User Input Error", Toast.LENGTH_LONG)
                    }
                    // Internet is not connected.
                    ErrorKeys.INTERNET_CONNECTION_ERROR -> {
                        Log.e(PaymentAPIActivity.TAG, "Internet Connection Error")
                        Toast.makeText(context, "Internet Connection Error", Toast.LENGTH_LONG)
                    }
                    // Server is not giving valid data.
                    ErrorKeys.DATA_PARSING_ERROR -> {
                        Log.e(PaymentAPIActivity.TAG, "Data Parsing Error")
                        Toast.makeText(context, "Data Parsing Error", Toast.LENGTH_LONG)
                    }
                    // User press back button or canceled the transaction.
                    ErrorKeys.CANCEL_TRANSACTION_ERROR -> {
                        Log.e(PaymentAPIActivity.TAG, "User Cancel The Transaction")
                        Toast.makeText(context, "User Cancel The Transaction", Toast.LENGTH_LONG)
                    }
                    // Server is not responding.
                    ErrorKeys.SERVER_ERROR -> {
                        Log.e(PaymentAPIActivity.TAG, "Server Error")
                        Toast.makeText(context, "Server Error", Toast.LENGTH_LONG)
                    }
                    // For some reason network is not responding
                    ErrorKeys.NETWORK_ERROR -> {
                        Log.e(PaymentAPIActivity.TAG, "Network Error")
                        Toast.makeText(context, "Network Error", Toast.LENGTH_LONG)
                    }
                }
            }
        })
    }
}