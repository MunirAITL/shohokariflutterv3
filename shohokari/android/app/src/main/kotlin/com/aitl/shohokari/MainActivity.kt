package com.shohokari

import android.media.MediaPlayer
import android.os.Bundle
import androidx.annotation.NonNull
import com.aitl.shohokari.AudioActivity
import com.aitl.shohokari.PaymentAPIActivity
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant


class MainActivity: FlutterActivity() {
    private val CHANNEL = "flutter.native/shohokari"
    var audioActivity = AudioActivity();

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)
        GeneratedPluginRegistrant.registerWith(flutterEngine);

        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, "android_app_retain").apply {
            setMethodCallHandler { method, result ->
                if (method.method == "sendToBackground") {
                    moveTaskToBack(true)
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MethodChannel(flutterEngine?.getDartExecutor(), CHANNEL).setMethodCallHandler { call, result ->
            if (call.method.equals("paymentAPIActivity")) {
                paymentAPIActivityCall(call, callback = {(it)
                    print(it);
                    result.success(it)
                });
            }
            else if (call.method.equals("audioActivity")) {
                audioActivityCall(call);
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun paymentAPIActivityCall(call: MethodCall, callback: (String) -> Unit) {
        var paymentAPIActivity = PaymentAPIActivity();
        paymentAPIActivity.paymentCardBySSLCommerzx(this, call, callback = {(it)
            print(it);
            callback.invoke(it)
        });
    }

    private fun audioActivityCall(call: MethodCall) {
        var method = call.argument<String>("method")
        when (method) {
            "start" -> audioActivity.start(this.baseContext, call)
            "stop" -> audioActivity.stop()
            else -> {
                print("audio method not found")
            }
        }
    }
}