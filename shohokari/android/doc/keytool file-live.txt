Store Pass
sH**00ca9ry%%ah@ma*!!#!o0O75

Alias
ShohoKari

smhafiz@aitl.net
shohokari_V1.11_070222.apk
=================================================================================
create keystore file
================================================================================================================
keytool -genkey -v -keystore shohokari.keystore -alias ShohoKari -keyalg RSA -keysize 2048 -validity 10000

create jks file
=================================================================================================================

keytool -genkey -v -keystore shohokari.jks -storetype JKS -keyalg RSA -keysize 2048 -validity 10000 -alias ShohoKari

The JKS keystore uses a proprietary format. It is recommended to migrate to PKCS12 which is an industry standard format using:
=================================================================================================================
keytool -importkeystore -srckeystore shohokari.jks -destkeystore shohokari.jks -deststoretype pkcs12

generate SSH1 Key
=================================================================================================================
keytool -keystore shohokari.jks -list -v

SHA1: 09:60:1B:CA:8D:72:BB:9F:29:47:42:1F:9B:AA:7E:BB:86:3A:88:49

======================================
FACEBOOK AUTH KEY:
keytool -exportcert -alias Mortgage_Magic -keystore mm.keystore | openssl sha1 -binary | openssl base64

full path:
F:\jdk\bin\keytool -exportcert -alias Mortgage_Magic -keystore E:\flutter_app\aitl\ht\herotaskerflutterappver3\HeroTaskerApp\android\doc\mm.keystore | openssl sha1 -binary | openssl base64

z9VPDwuPPKlmYIn8AC/c74qagxs=

========================================
For getting keystore file info
keytool -v -list -keystore mm.keystore
keystore file jks file password = ?
